import torch
from segmentation_models_pytorch.layers.out_modules import OutSoftmax, OutSigmoid
import matplotlib.pyplot as plt

from ..xview_base import XviewSegmentationLearner
from ..plotting import tensor_to_img, cmap, bounds, norm

prediction_softmax = OutSoftmax(take_argmax_flag=True)
prediction_sigmoid = OutSigmoid(threshold=0.5)
sigmoid = OutSigmoid(threshold=None)


class DamageSegmentationLearner(XviewSegmentationLearner):
    task = 'damage'

    def imshow_batch(self, pr, gt, x):

        if type(x) == list:
            x_pre_0 = x[0][0, :, :, :]
            x_post_0 = x[1][0, :, :, :]
        else:
            x_pre_0 = x[0, :, :, :]
            x_post_0 = x[0, :, :, :]

        gt_0 = gt[0, :, :, :]
        pr_0 = prediction_softmax(pr)[0, :, :, :]
        max_v_0, _ = torch.max(pr, dim=1)
        max_v_0 = max_v_0.unsqueeze(1).type(torch.cuda.FloatTensor)[0, :, :, :]

        x_pre_img = tensor_to_img(x_pre_0, mask=False, inverse_preprocess=True)
        x_post_img = tensor_to_img(x_post_0, mask=False, inverse_preprocess=True)

        gt_img = tensor_to_img(gt_0, mask=True)
        pr_img_where_gt = tensor_to_img(pr_0 * (gt_0 > 0).float(), mask=True)
        pr_img = tensor_to_img(pr_0, mask=True)
        pr_max_img = tensor_to_img(max_v_0, mask=True)

        fig, ax = plt.subplots(3, 2, figsize=(30, 40))

        fig.tight_layout(pad=5)
        im = ax[0, 0].imshow(x_pre_img); ax[0, 0].set_title('pre', fontsize=30)
        im = ax[0, 1].imshow(x_post_img); ax[0, 1].set_title('post', fontsize=30)
        im = ax[1, 0].imshow(gt_img, interpolation='nearest', cmap=cmap, norm=norm); ax[1, 0].set_title('gt', fontsize=30)
        im = ax[1, 1].imshow(pr_img_where_gt, interpolation='nearest', cmap=cmap, norm=norm); ax[1, 1].\
            set_title('pr where gt', fontsize=30)
        im = ax[2, 0].imshow(pr_max_img); ax[2, 0].set_title('max val (confidence)', fontsize=30)
        im = ax[2, 1].imshow(pr_img, interpolation='nearest', cmap=cmap, norm=norm); ax[2, 1].set_title('pr', fontsize=30)

        cbar_ax = fig.add_axes([0.95, 0.32, 0.05, 0.35])
        fig.colorbar(im, cmap=cmap, norm=norm, boundaries=bounds, cax=cbar_ax)
        plt.show()

