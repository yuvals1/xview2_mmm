from ..xview_base import to_numpy, to_tensor
from xview_mmm.xview_base import XviewLoss
import torch
from torch import nn
from torch.nn import functional as F
from .. import functions

reductions_fun = {'harmonic_mean': functions.harmonic_mean, 'arithmetic_mean': functions.arithmetic_mean}


def compute_reg_score(pr):
    return 0.0001 * torch.sum(pr[:, 0, :, :]) + 0.0001 * torch.sum(pr[:, 5, :, :])


class CE(XviewLoss):
    def __init__(self, weights):
        super().__init__()
        self.weights = weights
        self.ce = nn.CrossEntropyLoss(weight=self.weights)

    def forward(self, pr, gt):
        gt = gt.type(torch.cuda.LongTensor)
        h, w = gt.shape[-2], gt.shape[-1]
        gt = gt.view(-1, h, w)
        ce_loss = self.ce(pr, gt)
        return ce_loss


class FocalCE(XviewLoss):
    def __init__(self, weights, gamma):
        super().__init__()

        self.gamma = gamma if gamma else 0
        self.weights = weights
        self.ce = nn.CrossEntropyLoss(weight=self.weights)

    def forward(self, pr, gt):
        gt = gt.type(torch.cuda.LongTensor)
        h, w = gt.shape[-2], gt.shape[-1]
        gt = gt.view(-1, h, w)
        logpt = -F.cross_entropy(pr, gt, weight=self.weights, reduction='none')
        pt = torch.exp(logpt)

        class_weights_mask = 0
        for i in range(len(self.weights)):
            class_weights_mask += (gt == i).float() * self.weights[i]

        focal_mask = (1 - pt).pow(self.gamma)

        mask = class_weights_mask * focal_mask
        loss = mask * (-logpt)
        return loss.mean()


class Dice(XviewLoss):
    def __init__(self, weights, reduction='harmonic_mean', reg=False):
        super().__init__()

        self.softmax2d = torch.nn.Softmax2d()
        self.weights = weights
        self.reg = reg
        self.reduction_fun = reductions_fun[reduction]

    def forward(self, pr, gt):

        pr = self.softmax2d(pr)
        f_scores_lst = functions.multi_differentiable_f1_mask(pr, gt, self.weights)

        return 1 - self.reduction_fun(f_scores_lst, self.weights)


class FocalDice(XviewLoss):
    def __init__(self, weights, reduction='harmonic_mean', reg=False, gamma=3):
        super().__init__()
        self.softmax2d = torch.nn.Softmax2d()
        self.weights = weights
        self.reg = reg
        self.reduction_fun = reductions_fun[reduction]
        self.gamma = gamma

    def forward(self, pr, gt):

        pr = self.softmax2d(pr)
        f_scores_lst = functions.focal_multi_differentiable_f1_mask(pr, gt, self.weights, gamma=self.gamma)

        return 1 - self.reduction_fun(f_scores_lst, self.weights)


class DiceCE(XviewLoss):
    def __init__(self, weights_dice, weights_ce):
        super().__init__()
        self.multi_class_dice = Dice(weights=weights_dice)
        self.multi_class_ce = CE(weights=weights_ce)

    def forward(self, pr, gt):
        return 0.1 * self.multi_class_ce(pr, gt) + self.multi_class_dice(pr, gt)
