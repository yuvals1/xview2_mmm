from xview_mmm.xview_base.metrics.confusion_matrix import HarmonicF1Score, F1Score, Recall, Precision


class DamageScore(HarmonicF1Score):
    def __init__(self, activation='softmax2d'):
        super().__init__(activation=activation, labels=[1, 2, 3, 4])


class F1_1(F1Score):
    def __init__(self, activation='softmax2d'):
        super().__init__(activation=activation, labels=[1, 2, 3, 4], chosen_label=1)


class F1_2(F1Score):
    def __init__(self, activation='softmax2d'):
        super().__init__(activation=activation, labels=[1, 2, 3, 4], chosen_label=2)


class F1_3(F1Score):
    def __init__(self, activation='softmax2d'):
        super().__init__(activation=activation, labels=[1, 2, 3, 4], chosen_label=3)


class F1_4(F1Score):
    def __init__(self, activation='softmax2d'):
        super().__init__(activation=activation, labels=[1, 2, 3, 4], chosen_label=4)


class Recall1(Recall):
    def __init__(self, activation='softmax2d'):
        super().__init__(activation=activation, labels=[1, 2, 3, 4], chosen_label=1)


class Recall2(Recall):
    def __init__(self, activation='softmax2d'):
        super().__init__(activation=activation, labels=[1, 2, 3, 4], chosen_label=2)


class Recall3(Recall):
    def __init__(self, activation='softmax2d'):
        super().__init__(activation=activation, labels=[1, 2, 3, 4], chosen_label=3)


class Recall4(Recall):
    def __init__(self, activation='softmax2d'):
        super().__init__(activation=activation, labels=[1, 2, 3, 4], chosen_label=4)


class Precision1(Precision):
    def __init__(self, activation='softmax2d'):
        super().__init__(activation=activation, labels=[1, 2, 3, 4], chosen_label=1)


class Precision2(Precision):
    def __init__(self, activation='softmax2d'):
        super().__init__(activation=activation, labels=[1, 2, 3, 4], chosen_label=2)


class Precision3(Precision):
    def __init__(self, activation='softmax2d'):
        super().__init__(activation=activation, labels=[1, 2, 3, 4], chosen_label=3)


class Precision4(Precision):
    def __init__(self, activation='softmax2d'):
        super().__init__(activation=activation, labels=[1, 2, 3, 4], chosen_label=4)
