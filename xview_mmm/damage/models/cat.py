import torch
from segmentation_models_pytorch import Unet, FPN, Linknet, PSPNet


class CatUnet(Unet):
    def __init__(self, **kwargs):
        super().__init__(num_classes=6, in_channels=6, **kwargs)

    def forward(self, x):
        x = torch.cat(x, dim=1)
        return super().forward(x)


class CatFPN(FPN):
    def __init__(self, **kwargs):
        super().__init__(num_classes=6, in_channels=6, **kwargs)

    def forward(self, x):
        x = torch.cat(x, dim=1)
        return super().forward(x)


class CatLinknet(Linknet):
    def __init__(self, **kwargs):
        super().__init__(num_classes=6, in_channels=6, **kwargs)

    def forward(self, x):
        x = torch.cat(x, dim=1)
        return super().forward(x)


class CatPSPNet(PSPNet):
    def __init__(self, **kwargs):
        super().__init__(num_classes=6, in_channels=6, **kwargs)

    def forward(self, x):
        x = torch.cat(x, dim=1)
        return super().forward(x)

