from typing import Optional, Union, List
import torch
from segmentation_models_pytorch import SegmentationModel, SegmentationHead, \
    ClassificationHead, get_encoder
from segmentation_models_pytorch.unet.decoder import UnetDecoder


class SiamPlusOnPlusMaskUnet(SegmentationModel):
    def __init__(
        self,
        encoder_name: str = "resnet34",
        encoder_depth: int = 5,
        encoder1_weights: str = "imagenet",
        decoder_use_batchnorm: bool = True,
        decoder_channels: List[int] = (256, 128, 64, 32, 16),
        decoder_attention_type: Optional[str] = None,
        activation: Optional[Union[str, callable]] = None,
        aux_params: Optional[dict] = None,
        segmentation_head_kernel_size: int = 3,
    ):
        num_classes = 6
        in_channels_1 = 3
        in_channels_2 = 6
        in_channels_3 = 1
        super().__init__(num_classes=num_classes)

        self.encoder1 = get_encoder(
            encoder_name,
            in_channels=in_channels_1,
            depth=encoder_depth,
            weights=encoder1_weights,
        )
        self.encoder2 = get_encoder(
            encoder_name,
            in_channels=in_channels_2,
            depth=encoder_depth,
            weights=None,
        )
        self.encoder3 = get_encoder(
            encoder_name,
            in_channels=in_channels_3,
            depth=encoder_depth,
            weights=None,
        )
        self.decoder = UnetDecoder(
            encoder_channels=tuple([2 * x + y + z for x, y, z in zip(self.encoder1.out_channels,
                                                                     self.encoder2.out_channels,
                                                                     self.encoder3.out_channels)]),
            decoder_channels=decoder_channels,
            n_blocks=encoder_depth,
            use_batchnorm=decoder_use_batchnorm,
            center=True if encoder_name.startswith("vgg") else False,
            attention_type=decoder_attention_type,
        )

        self.segmentation_head = SegmentationHead(
            in_channels=decoder_channels[-1],
            out_channels=num_classes,
            activation=activation,
            kernel_size=segmentation_head_kernel_size,
        )

        if aux_params is not None:
            self.classification_head = ClassificationHead(
                in_channels=self.encoder.out_channels[-1], **aux_params
            )
        else:
            self.classification_head = None

        self.name = "u-{}".format(encoder_name)

    def forward(self, x):
        """Sequentially pass `x` trough model`s encoder, decoder and heads"""

        pre = x[0]
        post = x[1]
        mask = x[2]

        features1 = self.encoder1(pre)
        features2 = self.encoder1(post)
        features3 = self.encoder2(torch.cat([pre, post], dim=1))
        features4 = self.encoder3(mask)
        concat_features = [torch.cat([feat1, feat2, feat3, feat4], dim=1) for feat1, feat2, feat3, feat4 in
                           zip(features1, features2, features3, features4)]
        decoder_output = self.decoder(*concat_features)

        masks = self.segmentation_head(decoder_output)

        if self.classification_head is not None:
            labels = self.classification_head(concat_features[-1])
            return masks, labels

        return masks
