from typing import Optional, Union, List
import torch
from segmentation_models_pytorch import SegmentationModel, SegmentationHead, \
    ClassificationHead, get_encoder
from segmentation_models_pytorch.unet.decoder import UnetDecoder


class Wnet(SegmentationModel):
    def __init__(
        self,
        encoder_name: str = "resnet34",
        encoder_depth: int = 5,
        encoder1_weights: str = "imagenet",
        decoder_use_batchnorm: bool = True,
        decoder1_channels: List[int] = (256, 128, 64, 32, 32),
        decoder2_channels: List[int] = (256, 128, 64, 32, 16),
        decoder_attention_type: Optional[str] = None,
        activation: Optional[Union[str, callable]] = None,
        aux_params: Optional[dict] = None,
        segmentation_head_kernel_size: int = 3,
    ):
        num_classes = 6
        in_channels_1 = 6
        super().__init__(num_classes=num_classes)

        self.encoder1 = get_encoder(
            encoder_name,
            in_channels=in_channels_1,
            depth=encoder_depth,
            weights=encoder1_weights,
        )
        self.decoder1 = UnetDecoder(
            encoder_channels=self.encoder1.out_channels,
            decoder_channels=decoder1_channels,
            n_blocks=encoder_depth,
            use_batchnorm=decoder_use_batchnorm,
            center=True if encoder_name.startswith("vgg") else False,
            attention_type=decoder_attention_type,
        )

        self.encoder2 = get_encoder(
            encoder_name,
            in_channels=decoder1_channels[-1],
            depth=encoder_depth,
            weights=None,
        )

        self.decoder2 = UnetDecoder(
            encoder_channels=self.encoder2.out_channels,
            decoder_channels=decoder2_channels,
            n_blocks=encoder_depth,
            use_batchnorm=decoder_use_batchnorm,
            center=True if encoder_name.startswith("vgg") else False,
            attention_type=decoder_attention_type,
        )

        self.segmentation_head = SegmentationHead(
            in_channels=decoder2_channels[-1],
            out_channels=num_classes,
            activation=activation,
            kernel_size=segmentation_head_kernel_size,
        )

        if aux_params is not None:
            self.classification_head = ClassificationHead(
                in_channels=self.encoder2.out_channels[-1], **aux_params
            )
        else:
            self.classification_head = None

        self.name = "u-{}".format(encoder_name)

    def forward(self, x):
        """Sequentially pass `x` trough model`s encoder, decoder and heads"""

        pre_post = torch.cat([x[0], x[1]], dim=1)
        features1 = self.encoder1(pre_post)
        decoder1_output = self.decoder1(*features1)
        features2 = self.encoder2(decoder1_output)
        decoder2_output = self.decoder2(*features2)

        masks = self.segmentation_head(decoder2_output)

        if self.classification_head is not None:
            labels = self.classification_head(features2[-1])
            return masks, labels

        return masks
