from .losses import DiceCE

disasters = ['guatemala-volcano', 'hurricane-florence', 'hurricane-harvey',
             'hurricane-matthew', 'hurricane-michael', 'mexico-earthquake',
             'midwest-flooding', 'palu-tsunami', 'santa-rosa-wildfire',
             'socal-fire']

disaster_2_num = {k: i for i, k in zip(range(len(disasters)), disasters)}

