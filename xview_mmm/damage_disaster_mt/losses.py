from torch import nn
from ..damage import losses as dmg_losses
from ..xview_base import XviewLoss
from . import disaster_2_num


class DiceCE(XviewLoss):
    def __init__(self, weights):
        super().__init__()
        self.dmg_loss = dmg_losses.Dice(weights)
        self.ce_dis = nn.BCEWithLogitsLoss()
