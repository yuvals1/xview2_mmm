import torch
from torch import nn
from torch.nn import functional
import numpy as np


def calculate_batch_entropy_loss(pr, gt_inst):
    eps = 1e-4
    b = gt_inst.shape[0]
    h, w = gt_inst.shape[2], gt_inst.shape[3]
    # pr_log_pr = pr.softmax(dim=1) * pr.log_softmax(dim=1)

    loss_batch = 0
    for i in range(b):
        gt_inst_i = gt_inst[i:i+1, :, :, :]
        pr_i = pr[i:i+1, :, :, :]
        for j in range(1, int(gt_inst_i.max().item()) + 1):
            poly_mask = (gt_inst_i == j).type(torch.cuda.FloatTensor)
            pr_where_poly = pr_i * poly_mask
            mean_pr = pr_where_poly.sum(dim=3).sum(dim=2) / poly_mask.sum()
            mean_pr = mean_pr.unsqueeze(2).unsqueeze(3).repeat(1, 1, 1024, 1024) * poly_mask
            ce = (pr_where_poly.softmax(dim=1) + eps).log() * mean_pr
            ce_mean = ce.mean()
            loss_batch += ce_mean

    return loss_batch


def compute_polygon_mask(polygon, img):
    pass

#
# def create_mean_prob_mask(pr_b, gt_inst_b):
#     c, h, w = pr_b.shape[0], pr_b.shape[1], pr_b.shape[2]
#
#     means_mask = torch.zeros((1,c,h,w))
#     for i in range(1, int(gt_inst_b.max().item()) + 1):
#         poly_mask = (gt_inst_b == i).type(torch.cuda.FloatTensor)
#         pr_mean_where_poly = (pr_b * poly_mask).sum(dim=3).sum(dim=2) / poly_mask.sum()
#         pr_mean_where_poly = pr_mean_where_poly.unsqueeze(1).unsqueeze(2).repeat(1, h, w) * poly_mask
#         means_mask +=
#         a = post_mask * (post_mask_inst == i).type(torch.cuda.FloatTensor)
#         pr = (post_mask_inst == i).type(torch.cuda.FloatTensor)
#         imshow_mask(pr)
#         break