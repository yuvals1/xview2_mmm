import cv2
import numpy as np
from ..xview_base import to_numpy, to_tensor
from ..xview_base import XviewLoss
import torch
from torch import nn
from torch.nn import functional as F
from .. import functions


reductions_fun = {'harmonic_mean': functions.harmonic_mean, 'arithmetic_mean': functions.arithmetic_mean}


class FinalMetricDice(XviewLoss):
    def __init__(self, weights_dmg=torch.tensor([0, 1., 1., 1., 1., 0.], device='cuda'),
                 reduction='harmonic_mean', pre_post=False):
        super().__init__()

        self.softmax2d = torch.nn.Softmax2d()
        self.weights_dmg = weights_dmg
        self.reduction_fun = reductions_fun[reduction]
        self.pre_post = pre_post

    def forward(self, pr, gt):
        if self.pre_post:
            pre_gt = gt[0]
            post_gt = gt[1]
        else:
            pre_gt = gt
            post_gt = gt

        pr = self.softmax2d(pr)
        pr_0 = pr[:, 0, :, :].unsqueeze(1)
        gt_0_mask = (pre_gt == 0).float()
        loc_score = functions.differentiable_f1(1 - pr_0, 1 - gt_0_mask)

        f_scores_lst = functions.multi_differentiable_f1_mask(pr, post_gt, self.weights_dmg)
        dmg_score = self.reduction_fun(f_scores_lst, self.weights_dmg)

        return 1 - 0.3 * loc_score - 0.7 * dmg_score


class FinalMetricDiceFocal(XviewLoss):
    def __init__(self, reduction='harmonic_mean', alpha=1):
        super().__init__()

        self.softmax2d = torch.nn.Softmax2d()
        self.weights_dmg = torch.tensor([0, 1., 1., 1., 1., 0.], device='cuda')
        self.reduction_fun = reductions_fun[reduction]
        self.gamma = 1
        self.alpha = alpha

    def forward(self, pr, gt):
        pr = self.softmax2d(pr)
        pr_0 = pr[:, 0, :, :].unsqueeze(1)
        gt_0_mask = (gt == 0).float()
        loc_score = functions.differentiable_f1(1 - pr_0, 1 - gt_0_mask)

        f_scores_lst = functions.focal_multi_differentiable_f1_mask(pr, gt, self.weights_dmg, gamma=self.gamma)
        dmg_score = self.reduction_fun(f_scores_lst, self.weights_dmg)

        h, w = gt.shape[-2], gt.shape[-1]
        gt_new = gt.view(-1, h, w).type(torch.cuda.LongTensor)

        logpt = -F.cross_entropy(pr, gt_new, weight=torch.tensor([1., 1., 1., 1., 1., 0.], device='cuda'), reduction='none')
        pt = torch.exp(logpt)
        focal_mask = (1 - pt).pow(self.gamma)
        class_weights_mask = 0
        for i in range(5):
            class_weights_mask += (gt_new == i).float()

        focal_mask = class_weights_mask * focal_mask
        focal_mat = focal_mask * (-logpt)
        focal_loss = focal_mat.mean()

        return 1 - 0.3 * loc_score - 0.7 * dmg_score + self.alpha * focal_loss


class FinalMetricDiceWeight(XviewLoss):
    def __init__(self, reduction='harmonic_mean', loc_weight=0.3, dmg_weight=0.7):
        super().__init__()

        self.softmax2d = torch.nn.Softmax2d()
        self.weights_dmg = torch.tensor([0, 1., 1., 1., 1., 0.], device='cuda')
        self.reduction_fun = reductions_fun[reduction]
        self.dmg_weight = dmg_weight
        self.loc_weight = loc_weight
        self.gamma = 1

    def forward(self, pr, gt):
        pr = self.softmax2d(pr)
        pr_0 = pr[:, 0, :, :].unsqueeze(1)
        gt_0_mask = (gt == 0).float()
        loc_score = functions.differentiable_f1(1 - pr_0, 1 - gt_0_mask)

        f_scores_lst = functions.multi_differentiable_f1_mask(pr, gt, self.weights_dmg)
        dmg_score = self.reduction_fun(f_scores_lst, self.weights_dmg)

        return 1 - self.loc_weight * loc_score - self.dmg_weight * dmg_score


class Focal(XviewLoss):
    def __init__(self, weights=torch.tensor([1, 1., 1., 1., 1., 0.], device='cuda'), gamma=1):
        super().__init__()
        self.softmax2d = torch.nn.Softmax2d()
        self.weights = weights
        self.gamma = gamma

    def forward(self, pr, gt):
        h, w = gt.shape[-2], gt.shape[-1]
        gt = gt.view(-1, h, w).type(torch.cuda.LongTensor)

        logpt = -F.cross_entropy(pr, gt, weight=self.weights, reduction='none')
        pt = torch.exp(logpt)
        focal_mask = (1 - pt).pow(self.gamma)
        class_weights_mask = 0
        for i in range(5):
            class_weights_mask += (gt == i).float()

        focal_mask = class_weights_mask * focal_mask
        focal_mat = focal_mask * (-logpt)
        focal_loss = focal_mat.mean()

        return focal_loss


def make_single_contour_mask(gt):
    h, w = gt.shape
    a = np.zeros((h, w))
    contours, _ = cv2.findContours(gt, cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)

    for c in contours:
        cv2.drawContours(a, c, -1, 1, 3)

    a = 100 * cv2.blur(a, (27, 27))
    return a


def get_contours_mask(gt):

    b = gt.shape[0]
    tensors = []
    # gt_ = (gt > 0).type(torch.cuda.FloatTensor)
    for i in range(b):
        numpy_mask = to_numpy(gt[i, :, :, :].detach().cpu()).astype(float)
        contour_mask_numpy = make_single_contour_mask(numpy_mask[:, :, 0].astype('uint8'))
        contour_mask = to_tensor(contour_mask_numpy).cuda()
        tensors.append(contour_mask)

    return torch.stack(tensors, dim=0)


class FinalMetricContour(XviewLoss):
    def __init__(self, reduction='harmonic_mean', alpha=20):
        super().__init__()

        self.softmax2d = torch.nn.Softmax2d()
        self.weights_dmg = torch.tensor([0, 1., 1., 1., 1., 0.], device='cuda')
        self.reduction_fun = reductions_fun[reduction]
        self.gamma = 1
        self.alpha = alpha

    def forward(self, pr, gt):
        pr = self.softmax2d(pr)
        pr_0 = pr[:, 0, :, :].unsqueeze(1)
        gt_0_mask = (gt == 0).float()
        loc_score = functions.differentiable_f1(1 - pr_0, 1 - gt_0_mask)

        f_scores_lst = functions.focal_multi_differentiable_f1_mask(pr, gt, self.weights_dmg, gamma=self.gamma)
        dmg_score = self.reduction_fun(f_scores_lst, self.weights_dmg)

        h, w = gt.shape[-2], gt.shape[-1]

        contour_mask = get_contours_mask(gt)
        gt = gt.view(-1, h, w).type(torch.cuda.LongTensor)

        logpt = -F.cross_entropy(pr, gt, weight=torch.tensor([1., 1., 1., 1., 1., 0.], device='cuda'), reduction='none')

        class_weights_mask = 0
        for i in range(5):
            class_weights_mask += (gt == i).float()

        contour_mask = class_weights_mask * contour_mask
        loss_mat = contour_mask * (-logpt)
        bce_contour_loss = loss_mat.mean()

        return 0 * (1 - 0.3 * loc_score - 0.7 * dmg_score) + self.alpha * bce_contour_loss


class MSE(XviewLoss):
    def __init__(self):
        super().__init__()
        self.mse = nn.MSELoss()

    def forward(self, pr, gt):
        return self.mse(pr, gt)


def calculate_batch_entropy_loss(pr, gt_inst):
    eps = 1e-4
    b = gt_inst.shape[0]
    h, w = gt_inst.shape[2], gt_inst.shape[3]
    # pr_log_pr = pr.softmax(dim=1) * pr.log_softmax(dim=1)

    loss_batch = 0
    for i in range(b):
        gt_inst_i = gt_inst[i:i + 1, :, :, :]
        pr_i = pr[i:i + 1, :, :, :]
        for j in range(1, int(gt_inst_i.max().item()) + 1):
            poly_mask = (gt_inst_i == j).type(torch.cuda.FloatTensor)
            pr_where_poly = pr_i * poly_mask
            mean_pr = pr_where_poly.sum(dim=3).sum(dim=2) / poly_mask.sum()
            mean_pr = mean_pr.unsqueeze(2).unsqueeze(3).repeat(1, 1, h, w) * poly_mask
            ce = (pr_where_poly.softmax(dim=1) + eps).log() * mean_pr
            ce_mean = ce.mean()
            loss_batch += ce_mean

    return loss_batch

#
# class FinalMetricDiceEntropy(XviewLoss):
#     def __init__(self, weights_dmg=torch.tensor([0, 1., 1., 1., 1., 0.], device='cuda'),
#                  reduction='harmonic_mean', pre_post=False):
#         super().__init__()
#
#         self.softmax2d = torch.nn.Softmax2d()
#         self.weights_dmg = weights_dmg
#         self.reduction_fun = reductions_fun[reduction]
#         self.pre_post = pre_post
#
#     def forward(self, pr, gt):
#         gt_inst = gt[1]
#         gt = gt[0]
#
#         pr = self.softmax2d(pr)
#         pr_0 = pr[:, 0, :, :].unsqueeze(1)
#         gt_0_mask = (gt == 0).float()
#         loc_score = functions.differentiable_f1(1 - pr_0, 1 - gt_0_mask)
#
#         f_scores_lst = functions.multi_differentiable_f1_mask(pr, gt, self.weights_dmg)
#         dmg_score = self.reduction_fun(f_scores_lst, self.weights_dmg)
#
#         entropy = calculate_batch_entropy_loss(pr, gt_inst)
#
#         return 1 - 0.3 * loc_score - 0.7 * dmg_score + 100 * entropy


# import numpy as np
# from skimage.io import imshow
# from skimage.measure import label
# from scipy.ndimage.morphology import distance_transform_edt
# import numpy as np
#
#
# def unet_weight_map(y, wc=None, w0=10, sigma=5):
#     """
#     Generate weight maps as specified in the U-Net paper
#     for boolean mask.
#
#     "U-Net: Convolutional Networks for Biomedical Image Segmentation"
#     https://arxiv.org/pdf/1505.04597.pdf
#
#     Parameters
#     ----------
#     mask: Numpy array
#         2D array of shape (image_height, image_width) representing binary mask
#         of objects.
#     wc: dict
#         Dictionary of weight classes.
#     w0: int
#         Border weight parameter.
#     sigma: int
#         Border width parameter.
#
#     Returns
#     -------
#     Numpy array
#         Training weights. A 2D array of shape (image_height, image_width).
#     """
#
#     labels = label(y)
#     print(labels)
#     no_labels = labels == 0
#     label_ids = sorted(np.unique(labels))[1:]
#
#     if len(label_ids) > 1:
#         distances = np.zeros((y.shape[0], y.shape[1], len(label_ids)))
#
#         for i, label_id in enumerate(label_ids):
#             distances[:, :, i] = distance_transform_edt(labels != label_id)
#
#         distances = np.sort(distances, axis=2)
#         d1 = distances[:, :, 0]
#         d2 = distances[:, :, 1]
#         w = w0 * np.exp(-1 / 2 * ((d1 + d2) / sigma) ** 2) * no_labels
#     else:
#         w = np.zeros_like(y)
#     if wc:
#         class_weights = np.zeros_like(y)
#         for k, v in wc.items():
#             class_weights[y == k] = v
#         w = w + class_weights
#     return w
#
#
# def unet_on_batch(y, wc=None, w0=10, sigma=5):
#     b = y.shape[0]
#     tensors = []
#     for i in range(b):
#         numpy_mask = to_numpy(y[i, :, :, :].detach().cpu())
#         contour_mask_numpy = unet_weight_map(numpy_mask[:, :, 0].astype('uint8'), wc, w0, sigma)
#         contour_mask = to_tensor(contour_mask_numpy).cuda()
#         tensors.append(contour_mask)
#
#     return torch.stack(tensors, dim=0)
#
#
# class FinalMetricDiceWithBce(XviewLoss):
#     def __init__(self, reduction='harmonic_mean', pre_post=False):
#         super().__init__()
#
#         self.softmax2d = torch.nn.Softmax2d()
#         self.weights_dmg = torch.tensor([0, 1., 1., 1., 1., 0.], device='cuda')
#         self.reduction_fun = reductions_fun[reduction]
#         self.pre_post = pre_post
#
#     def forward(self, pr, gt):
#         if self.pre_post:
#             pre_gt = gt[0]
#             post_gt = gt[1]
#         else:
#             pre_gt = gt
#             post_gt = gt
#
#         pr = self.softmax2d(pr)
#         pr_0 = pr[:, 0, :, :].unsqueeze(1)
#         gt_0_mask = (pre_gt == 0).float()
#         loc_score1 = functions.differentiable_f1(1 - pr_0, 1 - gt_0_mask)
#         logpt = F.binary_cross_entropy_with_logits(1 - pr_0, 1 - gt_0_mask, reduction="none")
#         loc_score2 = unet_on_batch(1 - gt_0_mask, wc={1: 5, 0: 1}, w0=20, sigma=7) * logpt
#         loc_score = 0.3 * loc_score1 + 0.7 * loc_score2
#
#         f_scores_lst = functions.multi_differentiable_f1_mask(pr, post_gt, self.weights_dmg)
#         dmg_score = self.reduction_fun(f_scores_lst, self.weights_dmg)
#
#         return 1 - 0.3 * loc_score - 0.7 * dmg_score
