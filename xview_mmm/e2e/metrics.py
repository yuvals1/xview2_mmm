import numpy as np
from ..xview_base.metrics import ConfusionMatrix, f1_score, numpy_harmonic_mean, recall, precision, ConfusionMatrixReg


def loc_score_from_full_cm(cm):
    cm_loc = cm.copy()[:2, :2]
    cm_loc[0, 1] = np.sum(cm.copy()[0, 1:])
    cm_loc[1, 0] = np.sum(cm.copy()[1:, 0])
    cm_loc[1, 1] = np.sum(cm.copy()[1:, 1:])
    loc_score = f1_score(cm_loc, 1)
    return loc_score


def dmg_score_from_full_cm(cm):
    cm_dmg = cm.copy()
    cm_dmg[:, [0, 5]] = 0
    f_scores = []
    for label in [1, 2, 3, 4]:
        f_scores.append(f1_score(cm_dmg, label))
    dmg_score = numpy_harmonic_mean(f_scores)
    return dmg_score


def loc_precision_from_full_cm(cm):
    cm_loc = cm.copy()[:2, :2]
    cm_loc[0, 1] = np.sum(cm.copy()[0, 1:])
    cm_loc[1, 0] = np.sum(cm.copy()[1:, 0])
    cm_loc[1, 1] = np.sum(cm.copy()[1:, 1:])
    loc_score = precision(cm_loc, 1)
    return loc_score


def loc_recall_from_full_cm(cm):
    cm_loc = cm.copy()[:2, :2]
    cm_loc[0, 1] = np.sum(cm.copy()[0, 1:])
    cm_loc[1, 0] = np.sum(cm.copy()[1:, 0])
    cm_loc[1, 1] = np.sum(cm.copy()[1:, 1:])
    loc_score = recall(cm_loc, 1)
    return loc_score


def f1_per_label(cm, label):
    cm_dmg = cm.copy()
    cm_dmg[:, [0, 5]] = 0
    return f1_score(cm_dmg, label)


def precision_per_label(cm, label):
    cm_dmg = cm.copy()
    cm_dmg[:, [0, 5]] = 0
    return precision(cm_dmg, label)


def recall_per_label(cm, label):
    cm_dmg = cm.copy()
    cm_dmg[:, [0, 5]] = 0
    return recall(cm_dmg, label)


class E2EMetric(ConfusionMatrix):
    def __init__(self, activation='softmax2d', pre_post=False):
        super().__init__(activation, [0, 1, 2, 3, 4, 5], pre_post=pre_post)


class XviewScorePostReg(ConfusionMatrixReg):
    def epoch_update(self, batches_scores_list):
        cm = np.sum(batches_scores_list, axis=0)
        loc_score = loc_score_from_full_cm(cm)
        dmg_score = dmg_score_from_full_cm(cm)
        return 0.3 * loc_score + 0.7 * dmg_score


class LocalizationScoreReg(ConfusionMatrixReg):
    def epoch_update(self, batches_scores_list):
        cm = np.sum(batches_scores_list, axis=0)
        loc_score = loc_score_from_full_cm(cm)
        return loc_score


class DamageScoreReg(ConfusionMatrixReg):
    def epoch_update(self, batches_scores_list):
        cm = np.sum(batches_scores_list, axis=0)
        dmg_score = dmg_score_from_full_cm(cm)
        return dmg_score


class XviewScorePost(E2EMetric):
    def epoch_update(self, batches_scores_list):
        cm = np.sum(batches_scores_list, axis=0)
        loc_score = loc_score_from_full_cm(cm)
        dmg_score = dmg_score_from_full_cm(cm)
        return 0.3 * loc_score + 0.7 * dmg_score


class LocalizationScore(E2EMetric):
    def epoch_update(self, batches_scores_list):
        cm = np.sum(batches_scores_list, axis=0)
        loc_score = loc_score_from_full_cm(cm)
        return loc_score


class DamageScore(E2EMetric):
    def epoch_update(self, batches_scores_list):
        cm = np.sum(batches_scores_list, axis=0)
        dmg_score = dmg_score_from_full_cm(cm)
        return dmg_score


class LocalizationPrecision(E2EMetric):
    def epoch_update(self, batches_scores_list):
        cm = np.sum(batches_scores_list, axis=0)
        loc_score = loc_precision_from_full_cm(cm)
        return loc_score


class LocalizationRecall(E2EMetric):
    def epoch_update(self, batches_scores_list):
        cm = np.sum(batches_scores_list, axis=0)
        loc_score = loc_recall_from_full_cm(cm)
        return loc_score


class F1_1(E2EMetric):
    def epoch_update(self, batches_scores_list):
        cm = np.sum(batches_scores_list, axis=0)
        return f1_per_label(cm, 1)


class F1_2(E2EMetric):
    def epoch_update(self, batches_scores_list):
        cm = np.sum(batches_scores_list, axis=0)
        return f1_per_label(cm, 2)


class F1_3(E2EMetric):
    def epoch_update(self, batches_scores_list):
        cm = np.sum(batches_scores_list, axis=0)
        return f1_per_label(cm, 3)


class F1_4(E2EMetric):
    def epoch_update(self, batches_scores_list):
        cm = np.sum(batches_scores_list, axis=0)
        return f1_per_label(cm, 4)


class Precision1(E2EMetric):
    def epoch_update(self, batches_scores_list):
        cm = np.sum(batches_scores_list, axis=0)
        return precision_per_label(cm, 1)


class Precision2(E2EMetric):
    def epoch_update(self, batches_scores_list):
        cm = np.sum(batches_scores_list, axis=0)
        return precision_per_label(cm, 2)


class Precision3(E2EMetric):
    def epoch_update(self, batches_scores_list):
        cm = np.sum(batches_scores_list, axis=0)
        return precision_per_label(cm, 3)


class Precision4(E2EMetric):
    def epoch_update(self, batches_scores_list):
        cm = np.sum(batches_scores_list, axis=0)
        return precision_per_label(cm, 4)


class Recall1(E2EMetric):
    def epoch_update(self, batches_scores_list):
        cm = np.sum(batches_scores_list, axis=0)
        return recall_per_label(cm, 1)


class Recall2(E2EMetric):
    def epoch_update(self, batches_scores_list):
        cm = np.sum(batches_scores_list, axis=0)
        return recall_per_label(cm, 2)


class Recall3(E2EMetric):
    def epoch_update(self, batches_scores_list):
        cm = np.sum(batches_scores_list, axis=0)
        return recall_per_label(cm, 3)


class Recall4(E2EMetric):
    def epoch_update(self, batches_scores_list):
        cm = np.sum(batches_scores_list, axis=0)
        return recall_per_label(cm, 4)



