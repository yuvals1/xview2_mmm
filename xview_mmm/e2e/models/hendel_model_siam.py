from typing import Optional, Union, List
import torch
from torch import nn
from segmentation_models_pytorch import Unet, SegmentationModel, SegmentationHead, \
    ClassificationHead, get_encoder
from segmentation_models_pytorch.unet.decoder import UnetDecoder

from segmentation_models_pytorch.encoders.efficientnet import EfficientNetEncoder
from segmentation_models_pytorch.encoders import encoders, model_zoo


class HendelUnit(EfficientNetEncoder):
    def __init__(self, stage_idxs, out_channels, model_name, depth=5):
        super().__init__(stage_idxs, out_channels, model_name, depth)
        settings = encoders['efficientnet-b0']["pretrained_settings"]['imagenet']
        self.load_state_dict(model_zoo.load_url(settings["url"]))

        self.conv_ones = nn.ModuleList([nn.Identity(),
                                        nn.Conv2d(64, 32, kernel_size=1),
                                        nn.Conv2d(48, 24, kernel_size=1),
                                        nn.Conv2d(80, 40, kernel_size=1),
                                        nn.Conv2d(224, 112, kernel_size=1),
                                        nn.Conv2d(640, 320, kernel_size=1),
                                        ])

    def forward(self, pre_minus_post, pre_features, post_features):

        pre_post_features = [pre - post for pre, post in zip(pre_features, post_features)]
        features = [pre_minus_post]

        i = 0
        x = pre_post_features[i]
        i += 1
        if self._depth > 0:
            x = self._swish(self._bn0(self._conv_stem(x)))
            features.append(x)

        x = self.conv_ones[i](torch.cat([x, pre_post_features[i]], dim=1))
        i += 1
        if self._depth > 1:
            skip_connection_idx = 0
            for idx, block in enumerate(self._blocks):
                drop_connect_rate = self._global_params.drop_connect_rate
                if drop_connect_rate:
                    drop_connect_rate *= float(idx) / len(self._blocks)
                x = block(x, drop_connect_rate=drop_connect_rate)
                if idx == self._stage_idxs[skip_connection_idx] - 1:
                    skip_connection_idx += 1
                    features.append(x)
                    x = self.conv_ones[i](torch.cat([x, pre_post_features[i]], dim=1))
                    i += 1
                    if skip_connection_idx + 1 == self._depth:
                        break
        return features


class HendelModelSiam(SegmentationModel):
    def __init__(
            self,
            encoder_name: str = "resnet34",
            encoder_depth: int = 5,
            encoder_weights: str = "imagenet",
            decoder_use_batchnorm: bool = True,
            decoder_channels: List[int] = (256, 128, 64, 32, 16),
            decoder_attention_type: Optional[str] = None,
            activation: Optional[Union[str, callable]] = None,
            aux_params: Optional[dict] = None,
            segmentation_head_kernel_size: int = 3,
    ):
        num_classes = 6
        in_channels = 3
        super().__init__(num_classes=num_classes)

        self.encoder = get_encoder(
            encoder_name,
            in_channels=in_channels,
            depth=encoder_depth,
            weights=encoder_weights,
        )

        self.hendel_unit = HendelUnit(out_channels=(3, 32, 24, 40, 112, 320), stage_idxs=(3, 5, 9),
                                      model_name="efficientnet-b0", depth=5)

        encoders_out_channels = self.hendel_unit.out_channels
        self.decoder = UnetDecoder(
            encoder_channels=encoders_out_channels,
            decoder_channels=decoder_channels,
            n_blocks=encoder_depth,
            use_batchnorm=decoder_use_batchnorm,
            center=True if encoder_name.startswith("vgg") else False,
            attention_type=decoder_attention_type,
        )

        self.segmentation_head = SegmentationHead(
            in_channels=decoder_channels[-1],
            out_channels=num_classes,
            activation=activation,
            kernel_size=segmentation_head_kernel_size,
        )

        if aux_params is not None:
            self.classification_head = ClassificationHead(
                in_channels=encoders_out_channels[-1], **aux_params
            )
        else:
            self.classification_head = None

        self.name = "u-{}".format(encoder_name)

    def forward(self, x):
        """Sequentially pass `x` trough model`s encoder, decoder and heads"""
        pre = x[0]
        post = x[1]
        pre_features = self.encoder(pre)
        post_features = self.encoder(post)
        output = self.hendel_unit(pre - post, pre_features, post_features)

        output = self.decoder(*output)

        output = self.segmentation_head(output)

        return output
