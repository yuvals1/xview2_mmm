import torch
from segmentation_models_pytorch import Unet, FPN, Linknet, PSPNet


class UnetFromModels(Unet):
    def __init__(self, num_models, model1, model2, model3, **kwargs):
        super().__init__(num_classes=6, in_channels=6 * num_models, **kwargs)
        self.model1 = model1
        self.model2 = model2
        self.model3 = model3

    def forward(self, x):
        x1 = self.model1(x)
        x2 = self.model2(x)
        x3 = self.model3(x)
        x = torch.cat([x1, x2, x3], dim=1)
        return super().forward(x)


class CatUnet(Unet):
    def __init__(self, **kwargs):
        super().__init__(num_classes=6, in_channels=6, **kwargs)

    def forward(self, x):
        x = torch.cat(x, dim=1)
        return super().forward(x)
