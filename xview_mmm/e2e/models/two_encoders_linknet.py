from typing import Optional, Union, List
import torch
from segmentation_models_pytorch import SegmentationModel, SegmentationHead, Linknet,\
    ClassificationHead, get_encoder
from segmentation_models_pytorch.linknet.decoder import LinknetDecoder


class TwoEncodersLinknet(SegmentationModel):
    def __init__(
        self,
        encoder_name: str = "resnet34",
        encoder_depth: int = 5,
        encoder_weights: Optional[str] = "imagenet",
        in_channels=3,
        num_classes=6,
        decoder_use_batchnorm: bool = True,
        activation: Optional[Union[str, callable]] = None,
        aux_params: Optional[dict] = None,
    ):

        super().__init__(num_classes=num_classes)

        self.encoder1 = get_encoder(
            encoder_name,
            in_channels=in_channels,
            depth=encoder_depth,
            weights=encoder_weights,
        )

        self.encoder2 = get_encoder(
            encoder_name,
            in_channels=in_channels,
            depth=encoder_depth,
            weights=encoder_weights,
        )

        encoders_out_channels = tuple([x + y for x, y in zip(self.encoder1.out_channels, self.encoder2.out_channels)])
        self.decoder = LinknetDecoder(
            encoder_channels=encoders_out_channels,
            n_blocks=encoder_depth,
            prefinal_channels=32,
            use_batchnorm=decoder_use_batchnorm,
        )

        self.segmentation_head = SegmentationHead(
            in_channels=32, out_channels=num_classes, activation=activation, kernel_size=1
        )

        if aux_params is not None:
            self.classification_head = ClassificationHead(
                in_channels=self.encoder.out_channels[-1], **aux_params
            )
        else:
            self.classification_head = None

        self.name = "link-{}".format(encoder_name)

    def forward(self, x):
        """Sequentially pass `x` trough model`s encoder, decoder and heads"""
        pre = x[0]
        post = x[1]
        pre = self.encoder1(pre)
        post = self.encoder2(post)
        output = [torch.cat([feature1, feature2], dim=1) for feature1, feature2 in zip(pre, post)]
        output = self.decoder(*output)
        output = self.segmentation_head(output)
        return output

b= Linknet()
print(55)
a = TwoEncodersLinknet()