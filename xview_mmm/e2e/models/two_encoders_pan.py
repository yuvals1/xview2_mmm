from typing import Optional, Union, List
import torch
from segmentation_models_pytorch import Unet, SegmentationModel, SegmentationHead, \
    ClassificationHead, get_encoder
from segmentation_models_pytorch.pan.decoder import PANDecoder


class TwoEncodersPAN(SegmentationModel):
    def __init__(
            self,
            encoder_name: str = "resnet34",
            encoder_weights: str = "imagenet",
            encoder_depth=5,
            decoder_channels: int = 32,
            num_classes: int = 6,
            activation: Optional[Union[str, callable]] = None,
            upsampling: int = 4,
            aux_params: Optional[dict] = None
    ):
        in_channels = 3
        super().__init__(num_classes=num_classes)

        self.encoder1 = get_encoder(
            encoder_name,
            in_channels=in_channels,
            depth=encoder_depth,
            weights=encoder_weights,
        )

        self.encoder2 = get_encoder(
            encoder_name,
            in_channels=in_channels,
            depth=encoder_depth,
            weights=encoder_weights,
        )

        encoders_out_channels = tuple([x + y for x, y in zip(self.encoder1.out_channels, self.encoder2.out_channels)])
        self.decoder = PANDecoder(
            encoder_channels=encoders_out_channels,
            decoder_channels=decoder_channels,
        )

        self.segmentation_head = SegmentationHead(
            in_channels=decoder_channels,
            out_channels=num_classes,
            activation=activation,
            kernel_size=3,
            upsampling=upsampling
        )

        if aux_params is not None:
            self.classification_head = ClassificationHead(
                in_channels=self.encoder.out_channels[-1], **aux_params
            )
        else:
            self.classification_head = None

        self.name = "pan-{}".format(encoder_name)

    def forward(self, x):
        """Sequentially pass `x` trough model`s encoder, decoder and heads"""
        pre = x[0]
        post = x[1]
        pre = self.encoder1(pre)
        post = self.encoder2(post)
        concat_features = [torch.cat([feature1, feature2], dim=1) for feature1, feature2 in zip(pre, post)]
        out = self.decoder(*concat_features)

        out = self.segmentation_head(out)

        return out

