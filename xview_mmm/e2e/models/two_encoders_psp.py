from typing import Optional, Union, List
import torch
from segmentation_models_pytorch import Unet, SegmentationModel, SegmentationHead, \
    ClassificationHead, get_encoder
from segmentation_models_pytorch.pspnet.decoder import PSPDecoder


class TwoEncodersPSP(SegmentationModel):
    def __init__(
        self,
        encoder_name: str = "resnet34",
        encoder_weights: Optional[str] = "imagenet",
        encoder_depth: int = 3,
        psp_out_channels: int = 512,
        psp_use_batchnorm: bool = True,
        psp_dropout: float = 0,
        activation: Optional[Union[str, callable]] = None,
        upsampling: int = 8,
        aux_params: Optional[dict] = None,
    ):
        num_classes = 6
        in_channels = 3
        super().__init__(num_classes=num_classes)

        self.encoder1 = get_encoder(
            encoder_name,
            in_channels=in_channels,
            depth=encoder_depth,
            weights=encoder_weights,
        )

        self.encoder2 = get_encoder(
            encoder_name,
            in_channels=in_channels,
            depth=encoder_depth,
            weights=encoder_weights,
        )

        encoders_out_channels = tuple([x + y for x, y in zip(self.encoder1.out_channels, self.encoder2.out_channels)])
        self.decoder = PSPDecoder(
            encoder_channels=encoders_out_channels,
            use_batchnorm=psp_use_batchnorm,
            out_channels=psp_out_channels,
            dropout=psp_dropout,
        )

        self.segmentation_head = SegmentationHead(
            in_channels=psp_out_channels,
            out_channels=num_classes,
            kernel_size=3,
            activation=activation,
            upsampling=upsampling,
        )

        if aux_params:
            self.classification_head = ClassificationHead(
                in_channels=self.encoder.out_channels[-1], **aux_params
            )
        else:
            self.classification_head = None

        self.name = "psp-{}".format(encoder_name)

    def forward(self, x):
        """Sequentially pass `x` trough model`s encoder, decoder and heads"""
        pre = x[0]
        post = x[1]
        pre = self.encoder1(pre)
        post = self.encoder2(post)
        output = [torch.cat([feature1, feature2], dim=1) for feature1, feature2 in zip(pre, post)]
        output = self.decoder(*output)
        output = self.segmentation_head(output)
        return output
