from typing import Optional, Union, List
import torch
from torch import nn
from segmentation_models_pytorch import Unet, SegmentationModel, SegmentationHead, \
    ClassificationHead, get_encoder
from segmentation_models_pytorch.unet.decoder import UnetDecoder
from segmentation_models_pytorch.fpn.decoder import FPNDecoder


class TwoEncodersUnetFPN(SegmentationModel):
    def __init__(
        self,
        encoder_name: str = "resnet34",
        encoder_depth: int = 5,
        encoder_weights: str = "imagenet",
        unet_decoder_use_batchnorm: bool = True,
        unet_decoder_channels: List[int] = (256, 128, 64, 32, 16),
        unet_decoder_attention_type: Optional[str] = None,
        fpn_decoder_pyramid_channels: int = 256,
        fpn_decoder_segmentation_channels: int = 128,
        fpn_decoder_merge_policy: str = "add",
        fpn_decoder_dropout: float = 0.2,
        upsampling: int = 4,
        activation: Optional[Union[str, callable]] = None,
        aux_params: Optional[dict] = None,
        segmentation_head_kernel_size: int = 3,

    ):
        num_classes = 6
        in_channels = 3
        super().__init__(num_classes=num_classes)

        self.encoder1 = get_encoder(
            encoder_name,
            in_channels=in_channels,
            depth=encoder_depth,
            weights=encoder_weights,
        )

        self.encoder2 = get_encoder(
            encoder_name,
            in_channels=in_channels,
            depth=encoder_depth,
            weights=encoder_weights,
        )

        encoders_out_channels = tuple([x + y for x, y in zip(self.encoder1.out_channels, self.encoder2.out_channels)])

        self.unet_decoder = UnetDecoder(
            encoder_channels=encoders_out_channels,
            decoder_channels=unet_decoder_channels,
            n_blocks=encoder_depth,
            use_batchnorm=unet_decoder_use_batchnorm,
            center=True if encoder_name.startswith("vgg") else False,
            attention_type=unet_decoder_attention_type,
        )

        self.fpn_decoder = FPNDecoder(
            encoder_channels=encoders_out_channels,
            encoder_depth=encoder_depth,
            pyramid_channels=fpn_decoder_pyramid_channels,
            segmentation_channels=fpn_decoder_segmentation_channels,
            dropout=fpn_decoder_dropout,
            merge_policy=fpn_decoder_merge_policy,
        )

        self.upsampling = nn.UpsamplingBilinear2d(scale_factor=upsampling)

        head_input_channels = unet_decoder_channels[-1] + self.fpn_decoder.out_channels
        self.segmentation_head = SegmentationHead(
            in_channels=head_input_channels,
            out_channels=num_classes,
            activation=activation,
            kernel_size=segmentation_head_kernel_size,
        )

        if aux_params is not None:
            self.classification_head = ClassificationHead(
                in_channels=encoders_out_channels[-1], **aux_params
            )
        else:
            self.classification_head = None

        self.name = "u-{}".format(encoder_name)

    def forward(self, x):
        """Sequentially pass `x` trough model`s encoder, decoder and heads"""
        pre = x[0]
        post = x[1]
        pre = self.encoder1(pre)
        post = self.encoder2(post)
        output = [torch.cat([feature1, feature2], dim=1) for feature1, feature2 in zip(pre, post)]
        del pre
        del post
        output1 = self.unet_decoder(*output)
        output2 = self.fpn_decoder(*output)
        output2 = self.upsampling(output2)

        output = torch.cat([output1, output2], dim=1)
        del output1
        del output2
        output = self.segmentation_head(output)
        return output
