from typing import Optional, Union, List
import torch
from segmentation_models_pytorch import Unet, SegmentationModel, SegmentationHead, \
    ClassificationHead, get_encoder
from segmentation_models_pytorch.unet.decoder import UnetDecoder, UnetDecoderAll
from torch.nn import functional as F

#
# def concat_lists(*ls):
#     lst = ls[0].pop(0)
#     for i in range(len(ls[-1])):
#         for ls
#         feat1 = long_lst.pop(0)
#         feat2 = short_lst.pop(0)
#         lst.append(torch.cat([feat1, feat2], dim=1))
#
#     lst = lst + [long_lst.pop(0), long_lst.pop(0)]
#     return lst


class TwoEncodersUnetPlusPlus2(SegmentationModel):
    def __init__(
            self,
            encoder_name: str = "resnet34",
            encoder_depth: int = 5,
            encoder_weights: str = "imagenet",
            decoder_use_batchnorm: bool = True,
            decoder_channels: List[int] = (256, 128, 64, 32, 16),
            decoder_attention_type: Optional[str] = None,
            activation: Optional[Union[str, callable]] = None,
            aux_params: Optional[dict] = None,
            segmentation_head_kernel_size: int = 3,
            upsampling=1,
            num_classes=6,
            ds=False,
    ):
        in_channels = 3
        super().__init__(num_classes=num_classes)
        self.ds = ds

        self.encoder1 = get_encoder(
            encoder_name,
            in_channels=in_channels,
            depth=encoder_depth,
            weights=encoder_weights,
        )

        self.encoder2 = get_encoder(
            encoder_name,
            in_channels=in_channels,
            depth=encoder_depth,
            weights=encoder_weights,
        )

        one_enc_out_channels = self.encoder1.out_channels
        efficientnet_b0_encoder_in_ch = [3, 32, 24, 40, 112, 320]

        decoder1_in_ch = [2*3, 2*32, 2*24]
        decoder1_out_ch = [32, 16]

        decoder2_in_ch = [2*3, 3*32, 2*24, 2*40]
        decoder2_out_ch = [24, 32, 16]

        decoder3_in_ch = [2*3, 4*32, 3*24, 2*40, 2*112]
        decoder3_out_ch = [40, 24, 32, 16]

        decoder4_in_ch = [2*3, 5*32, 4*24, 3*40, 2*112, 2*320]
        # decoder4_out_ch = tuple([1 * x for x in one_enc_out_channels])[1:6]

        self.decoder1 = UnetDecoderAll(
            encoder_channels=decoder1_in_ch,
            decoder_channels=decoder1_out_ch,
            n_blocks=2,
            use_batchnorm=decoder_use_batchnorm,
            center=True if encoder_name.startswith("vgg") else False,
            attention_type=decoder_attention_type,
        )
        self.decoder2 = UnetDecoderAll(
            encoder_channels=decoder2_in_ch,
            decoder_channels=decoder2_out_ch,
            n_blocks=3,
            use_batchnorm=decoder_use_batchnorm,
            center=True if encoder_name.startswith("vgg") else False,
            attention_type=decoder_attention_type,
        )
        self.decoder3 = UnetDecoderAll(
            encoder_channels=decoder3_in_ch,
            decoder_channels=decoder3_out_ch,
            n_blocks=4,
            use_batchnorm=decoder_use_batchnorm,
            center=True if encoder_name.startswith("vgg") else False,
            attention_type=decoder_attention_type,
        )
        self.decoder4 = UnetDecoder(
            encoder_channels=decoder4_in_ch,
            decoder_channels=decoder_channels,
            n_blocks=5,
            use_batchnorm=decoder_use_batchnorm,
            center=True if encoder_name.startswith("vgg") else False,
            attention_type=decoder_attention_type,
        )
        self.segmentation_head = SegmentationHead(
            in_channels=decoder_channels[-1],
            out_channels=num_classes,
            activation=activation,
            kernel_size=segmentation_head_kernel_size,
            upsampling=upsampling,
        )

        self.name = "u-{}".format(encoder_name)

    def forward(self, x):
        """Sequentially pass `x` trough model`s encoder, decoder and heads"""
        pre = x[0]
        post = x[1]
        pre = self.encoder1(pre)
        post = self.encoder2(post)
        concat_pre_post = [torch.cat([feature1, feature2], dim=1) for feature1, feature2 in zip(pre, post)]

        in_decoder1 = concat_pre_post[0:3]
        out_decoder1 = self.decoder1(*in_decoder1)
        out_decoder1 = out_decoder1[::-1]

        in_decoder2 = concat_pre_post[0:4]
        in_decoder2[1] = torch.cat([in_decoder2[1], out_decoder1[1]], dim=1)
        out_decoder2 = self.decoder2(*in_decoder2)
        out_decoder2 = out_decoder2[::-1]

        in_decoder3 = concat_pre_post[0:5]
        in_decoder3[1] = torch.cat([in_decoder3[1], out_decoder2[1], out_decoder1[1]], dim=1)
        in_decoder3[2] = torch.cat([in_decoder3[2], out_decoder2[2]], dim=1)
        out_decoder3 = self.decoder3(*in_decoder3)
        out_decoder3 = out_decoder3[::-1]

        in_decoder4 = concat_pre_post[0:6]
        in_decoder4[1] = torch.cat([in_decoder4[1], out_decoder3[1], out_decoder2[1], out_decoder1[1]], dim=1)
        in_decoder4[2] = torch.cat([in_decoder4[2], out_decoder3[2], out_decoder2[2]], dim=1)
        in_decoder4[3] = torch.cat([in_decoder4[3], out_decoder3[3]], dim=1)
        out_decoder4 = self.decoder4(*in_decoder4)

        out = self.segmentation_head(out_decoder4)

        return out
