import torch
from torch import nn
from unet_family.layers import UnetUp
from unet_family.utils import init_weights
from segmentation_models_pytorch import get_encoder
import torch.nn.functional as F


class UnetPP(nn.Module):

    def __init__(self,
                 encoder_name: str = "resnet34",
                 encoder_depth=5,
                 encoder_weights: str = "imagenet",
                 in_channels=3,
                 n_classes=6,
                 feature_scale=2,
                 is_deconv=True,
                 is_batchnorm=True,
                 is_ds=True):

        super().__init__()
        self.in_channels = in_channels
        self.feature_scale = feature_scale
        self.is_deconv = is_deconv
        self.is_batchnorm = is_batchnorm
        self.is_ds = is_ds

        # filters = [64, 128, 256, 512, 1024]
        # filters = [int(x / self.feature_scale) for x in filters]


        # downsampling
        self.encoder = get_encoder(
            encoder_name,
            in_channels=in_channels,
            depth=encoder_depth,
            weights=encoder_weights,
        )

        # self.maxpool = nn.MaxPool2d(kernel_size=2)
        # self.conv00 = unetConv2(self.in_channels, filters[0], self.is_batchnorm)
        # self.conv10 = unetConv2(filters[0], filters[1], self.is_batchnorm)
        # self.conv20 = unetConv2(filters[1], filters[2], self.is_batchnorm)
        # self.conv30 = unetConv2(filters[2], filters[3], self.is_batchnorm)
        # self.conv40 = unetConv2(filters[3], filters[4], self.is_batchnorm)

        # encoders_out_channels = tuple([x + y for x, y in zip(self.encoder1.out_channels, self.encoder2.out_channels)])
        encoders_out_channels = self.encoder.out_channels
        filters = encoders_out_channels[1:]

        # upsampling
        self.up_concat01 = UnetUp(filters[1], filters[0], self.is_deconv)
        self.up_concat11 = UnetUp(filters[2], filters[1], self.is_deconv)
        self.up_concat21 = UnetUp(filters[3], filters[2], self.is_deconv)
        self.up_concat31 = UnetUp(filters[4], filters[3], self.is_deconv)

        self.up_concat02 = UnetUp(filters[1], filters[0], self.is_deconv, 3)
        self.up_concat12 = UnetUp(filters[2], filters[1], self.is_deconv, 3)
        self.up_concat22 = UnetUp(filters[3], filters[2], self.is_deconv, 3)

        self.up_concat03 = UnetUp(filters[1], filters[0], self.is_deconv, 4)
        self.up_concat13 = UnetUp(filters[2], filters[1], self.is_deconv, 4)

        self.up_concat04 = UnetUp(filters[1], filters[0], self.is_deconv, 5)

        # final conv (without any concat)
        self.final_1 = nn.Conv2d(filters[0], n_classes, 1)
        self.final_2 = nn.Conv2d(filters[0], n_classes, 1)
        self.final_3 = nn.Conv2d(filters[0], n_classes, 1)
        self.final_4 = nn.Conv2d(filters[0], n_classes, 1)

        # initialise weights
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                init_weights(m, init_type='kaiming')
            elif isinstance(m, nn.BatchNorm2d):
                init_weights(m, init_type='kaiming')

    def forward(self, x):
        # column : 0
        out_encoder = self.encoder(x)
        X_00, X_10, X_20, X_30, X_40 = out_encoder[1:]


        X_01 = self.up_concat01(X_10, X_00)
        X_11 = self.up_concat11(X_20, X_10)
        X_21 = self.up_concat21(X_30, X_20)
        X_31 = self.up_concat31(X_40, X_30)
        # column : 2
        X_02 = self.up_concat02(X_11, X_00, X_01)
        X_12 = self.up_concat12(X_21, X_10, X_11)
        X_22 = self.up_concat22(X_31, X_20, X_21)
        # column : 3
        X_03 = self.up_concat03(X_12, X_00, X_01, X_02)
        X_13 = self.up_concat13(X_22, X_10, X_11, X_12)
        # column : 4
        X_04 = self.up_concat04(X_13, X_00, X_01, X_02, X_03)

        # final layer
        final_1 = F.interpolate(X_01, scale_factor=2, mode="nearest")
        final_1 = self.final_1(final_1)
        final_2 = F.interpolate(X_02, scale_factor=2, mode="nearest")
        final_2 = self.final_2(final_2)
        final_3 = F.interpolate(X_03, scale_factor=2, mode="nearest")
        final_3 = self.final_3(final_3)
        final_4 = F.interpolate(X_04, scale_factor=2, mode="nearest")
        final_4 = self.final_4(final_4)

        final = (final_1 + final_2 + final_3 + final_4) / 4

        if self.is_ds:
            return final
        else:
            return final_4


