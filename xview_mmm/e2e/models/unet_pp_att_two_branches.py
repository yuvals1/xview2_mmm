import torch
import torch.nn as nn
from unet_family.layers import UnetConv2, UnetUpTwoEnc
from unet_family.utils import init_weights, count_param
from segmentation_models_pytorch import SegmentationModel, get_encoder
import torch.nn.functional as F
from att_models.layers.grid_attention_layer import GridAttentionBlock2D
from att_models.networks.utils import UnetGridGatingSignal2

class UpsampleDecoder(nn.Module):
    def __init__(self, filters, num_classes, is_ds=False, is_deconv=False, is_batchnorm=True,
                 nonlocal_mode='concatenation', attention_dsample=(2,2)):
        super().__init__()

        self.gating = UnetGridGatingSignal2(filters[4], filters[3], kernel_size=(1, 1), is_batchnorm=is_batchnorm)

        self.attentionblock1 = GridAttentionBlock2D(in_channels=filters[0], gating_channels=filters[3],
                                                    inter_channels=filters[0], sub_sample_factor=attention_dsample,
                                                    mode=nonlocal_mode)

        self.attentionblock2 = GridAttentionBlock2D(in_channels=filters[1], gating_channels=filters[3],
                                                    inter_channels=filters[1], sub_sample_factor=attention_dsample,
                                                    mode=nonlocal_mode)
        self.attentionblock3 = GridAttentionBlock2D(in_channels=filters[2], gating_channels=filters[3],
                                                    inter_channels=filters[2], sub_sample_factor=attention_dsample,
                                                    mode=nonlocal_mode)
        self.attentionblock4 = GridAttentionBlock2D(in_channels=filters[3], gating_channels=filters[3],
                                                    inter_channels=filters[3], sub_sample_factor=attention_dsample,
                                                    mode=nonlocal_mode)

        self.up_concat01 = UnetUpTwoEnc(filters[1], filters[0], is_deconv, n_diag=1)
        self.up_concat11 = UnetUpTwoEnc(filters[2], filters[1], is_deconv, n_diag=1)
        self.up_concat21 = UnetUpTwoEnc(filters[3], filters[2], is_deconv, n_diag=1)
        self.up_concat31 = UnetUpTwoEnc(filters[4], filters[3], is_deconv, n_diag=1)

        self.up_concat02 = UnetUpTwoEnc(filters[1], filters[0], is_deconv, n_diag=2)
        self.up_concat12 = UnetUpTwoEnc(filters[2], filters[1], is_deconv, n_diag=2)
        self.up_concat22 = UnetUpTwoEnc(filters[3], filters[2], is_deconv, n_diag=2)

        self.up_concat03 = UnetUpTwoEnc(filters[1], filters[0], is_deconv, n_diag=3)
        self.up_concat13 = UnetUpTwoEnc(filters[2], filters[1], is_deconv, n_diag=3)

        self.up_concat04 = UnetUpTwoEnc(filters[1], filters[0], is_deconv, n_diag=4)

        # final conv (without any concat)
        self.final_1 = nn.Conv2d(filters[0], num_classes, 1)
        self.final_2 = nn.Conv2d(filters[0], num_classes, 1)
        self.final_3 = nn.Conv2d(filters[0], num_classes, 1)
        self.final_4 = nn.Conv2d(filters[0], num_classes, 1)

        self.is_ds = is_ds

    def forward(self, concat_features):
        X_00, X_10, X_20, X_30, X_40 = concat_features[1:]
        gating = self.gating(X_40)

        # Attention Mechanism
        # filters[-1] = X_40
        # filters[-2], att4 = self.attentionblock4(X_30, gating)
        # filters[-3], att3 = self.attentionblock3(X_20, gating)
        # filters[-4], att2 = self.attentionblock2(X_10, gating)

        att_X01 = self.attentionblock1(X_00, gating)
        X_01 = self.up_concat01(X_10, att_X01)
        att_X11 = self.attentionblock2(X_10, gating)
        X_11 = self.up_concat11(X_20, att_X11)
        att_X21 = self.attentionblock3(X_20, gating)
        X_21 = self.up_concat21(X_30, att_X21)
        att_X31 = self.attentionblock4(X_30, gating)
        X_31 = self.up_concat31(X_40, att_X31)
        # column : 2
        X_02 = self.up_concat02(X_11, X_00, X_01)
        X_12 = self.up_concat12(X_21, X_10, X_11)
        X_22 = self.up_concat22(X_31, X_20, X_21)
        # column : 3
        X_03 = self.up_concat03(X_12, X_00, X_01, X_02)
        X_13 = self.up_concat13(X_22, X_10, X_11, X_12)
        # column : 4
        X_04 = self.up_concat04(X_13, X_00, X_01, X_02, X_03)

        final_4 = F.interpolate(X_04, scale_factor=2, mode="nearest")
        final_4 = self.final_4(final_4)

        if self.is_ds:
            final_1 = F.interpolate(X_01, scale_factor=2, mode="nearest")
            final_1 = self.final_1(final_1)
            final_2 = F.interpolate(X_02, scale_factor=2, mode="nearest")
            final_2 = self.final_2(final_2)
            final_3 = F.interpolate(X_03, scale_factor=2, mode="nearest")
            final_3 = self.final_3(final_3)
            final = (final_1 + final_2 + final_3 + final_4) / 4
            return final
        else:
            return final_4


class UnetPPAttTwoEncoders(SegmentationModel):
    def __init__(self,
                 encoder_name,
                 encoder_weights,
                 encoder_depth=5,
                 in_channels=3,
                 num_classes=6,
                 feature_scale=2,
                 is_deconv=False,
                 is_batchnorm=True,
                 is_ds=False,):

        super().__init__(num_classes=num_classes)
        self.in_channels = in_channels
        self.feature_scale = feature_scale
        self.is_deconv = is_deconv
        self.is_batchnorm = is_batchnorm
        self.is_ds = is_ds

        # downsampling
        self.maxpool = nn.MaxPool2d(kernel_size=2)
        self.encoder1 = get_encoder(
            encoder_name,
            in_channels=in_channels,
            depth=encoder_depth,
            weights=encoder_weights,
        )

        self.encoder2 = get_encoder(
            encoder_name,
            in_channels=in_channels,
            depth=encoder_depth,
            weights=encoder_weights,
        )

        encoders_out_channels = self.encoder1.out_channels
        filters = encoders_out_channels[1:]

        # upsampling

        self.decoder = UpsampleDecoder(filters, num_classes, is_ds, is_deconv, is_batchnorm=is_batchnorm)

        # initialise weights
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                init_weights(m, init_type='kaiming')
            elif isinstance(m, nn.BatchNorm2d):
                init_weights(m, init_type='kaiming')

    def forward(self, x):
        pre = x[0]
        post = x[1]

        pre = self.encoder1(pre)
        post = self.encoder1(post)
        concat_features = [torch.cat([feature1, feature2], dim=1) for feature1, feature2 in zip(pre, post)]

        final = self.decoder(concat_features)

        return final
