import torch
import torch.nn as nn
from unet_family.layers import UnetConv2, UnetUpTwoEnc
from unet_family.utils import init_weights, count_param
from segmentation_models_pytorch import SegmentationModel


class UnetPPTwoEncodersBasic(SegmentationModel):
    def __init__(self,
                 in_channels=3,
                 num_classes=6,
                 feature_scale=2,
                 is_deconv=True,
                 is_batchnorm=True,
                 is_ds=True):

        super().__init__(num_classes=num_classes)
        self.in_channels = in_channels
        self.feature_scale = feature_scale
        self.is_deconv = is_deconv
        self.is_batchnorm = is_batchnorm
        self.is_ds = is_ds

        filters = [64, 128, 256, 512, 1024]
        filters = [int(x / self.feature_scale) for x in filters]

        # downsampling
        self.maxpool = nn.MaxPool2d(kernel_size=2)
        self.conv00_pre = UnetConv2(self.in_channels, filters[0], self.is_batchnorm)
        self.conv10_pre = UnetConv2(filters[0], filters[1], self.is_batchnorm)
        self.conv20_pre = UnetConv2(filters[1], filters[2], self.is_batchnorm)
        self.conv30_pre = UnetConv2(filters[2], filters[3], self.is_batchnorm)
        self.conv40_pre = UnetConv2(filters[3], filters[4], self.is_batchnorm)

        self.conv00_post = UnetConv2(self.in_channels, filters[0], self.is_batchnorm)
        self.conv10_post = UnetConv2(filters[0], filters[1], self.is_batchnorm)
        self.conv20_post = UnetConv2(filters[1], filters[2], self.is_batchnorm)
        self.conv30_post = UnetConv2(filters[2], filters[3], self.is_batchnorm)
        self.conv40_post = UnetConv2(filters[3], filters[4], self.is_batchnorm)

        # upsampling
        self.up_concat01 = UnetUpTwoEnc(filters[1], filters[0], self.is_deconv, n_diag=1)
        self.up_concat11 = UnetUpTwoEnc(filters[2], filters[1], self.is_deconv, n_diag=1)
        self.up_concat21 = UnetUpTwoEnc(filters[3], filters[2], self.is_deconv, n_diag=1)
        self.up_concat31 = UnetUpTwoEnc(filters[4], filters[3], self.is_deconv, n_diag=1)

        self.up_concat02 = UnetUpTwoEnc(filters[1], filters[0], self.is_deconv, n_diag=2)
        self.up_concat12 = UnetUpTwoEnc(filters[2], filters[1], self.is_deconv, n_diag=2)
        self.up_concat22 = UnetUpTwoEnc(filters[3], filters[2], self.is_deconv, n_diag=2)

        self.up_concat03 = UnetUpTwoEnc(filters[1], filters[0], self.is_deconv, n_diag=3)
        self.up_concat13 = UnetUpTwoEnc(filters[2], filters[1], self.is_deconv, n_diag=3)

        self.up_concat04 = UnetUpTwoEnc(filters[1], filters[0], self.is_deconv, n_diag=4)

        # final conv (without any concat)
        self.final_1 = nn.Conv2d(filters[0], num_classes, 1)
        self.final_2 = nn.Conv2d(filters[0], num_classes, 1)
        self.final_3 = nn.Conv2d(filters[0], num_classes, 1)
        self.final_4 = nn.Conv2d(filters[0], num_classes, 1)

        # initialise weights
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                init_weights(m, init_type='kaiming')
            elif isinstance(m, nn.BatchNorm2d):
                init_weights(m, init_type='kaiming')

    def forward(self, x):
        # column : 0
        pre = x[0]
        post = x[1]

        X_00_pre = self.conv00_pre(pre)  # 16*512*512
        maxpool0 = self.maxpool(X_00_pre)  # 16*256*256
        X_10_pre = self.conv10_pre(maxpool0)  # 32*256*256
        maxpool1 = self.maxpool(X_10_pre)  # 32*128*128
        X_20_pre = self.conv20_pre(maxpool1)  # 64*128*128
        maxpool2 = self.maxpool(X_20_pre)  # 64*64*64
        X_30_pre = self.conv30_pre(maxpool2)  # 128*64*64
        maxpool3 = self.maxpool(X_30_pre)  # 128*32*32
        X_40_pre = self.conv40_pre(maxpool3)  # 256*32*32
        # column : 1

        X_00_post = self.conv00_post(post)  # 16*512*512
        maxpool0 = self.maxpool(X_00_post)  # 16*256*256
        X_10_post = self.conv10_post(maxpool0)  # 32*256*256
        maxpool1 = self.maxpool(X_10_post)  # 32*128*128
        X_20_post = self.conv20_post(maxpool1)  # 64*128*128
        maxpool2 = self.maxpool(X_20_post)  # 64*64*64
        X_30_post = self.conv30_post(maxpool2)  # 128*64*64
        maxpool3 = self.maxpool(X_30_post)  # 128*32*32
        X_40_post = self.conv40_post(maxpool3)  # 256*32*32

        X_00 = torch.cat([X_00_pre, X_00_post], dim=1)
        X_10 = torch.cat([X_10_pre, X_10_post], dim=1)
        X_20 = torch.cat([X_20_pre, X_20_post], dim=1)
        X_30 = torch.cat([X_30_pre, X_30_post], dim=1)
        X_40 = torch.cat([X_40_pre, X_40_post], dim=1)

        X_01 = self.up_concat01(X_10, X_00)
        X_11 = self.up_concat11(X_20, X_10)
        X_21 = self.up_concat21(X_30, X_20)
        X_31 = self.up_concat31(X_40, X_30)
        # column : 2
        X_02 = self.up_concat02(X_11, X_00, X_01)
        X_12 = self.up_concat12(X_21, X_10, X_11)
        X_22 = self.up_concat22(X_31, X_20, X_21)
        # column : 3
        X_03 = self.up_concat03(X_12, X_00, X_01, X_02)
        X_13 = self.up_concat13(X_22, X_10, X_11, X_12)
        # column : 4
        X_04 = self.up_concat04(X_13, X_00, X_01, X_02, X_03)

        # final layer

        final_4 = self.final_4(X_04)

        if self.is_ds:
            final_1 = self.final_1(X_01)
            final_2 = self.final_2(X_02)
            final_3 = self.final_3(X_03)
            final = (final_1 + final_2 + final_3 + final_4) / 4
            return final
        else:
            return final_4


