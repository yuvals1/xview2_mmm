import torch
from segmentation_models_pytorch.layers.out_modules import OutSoftmax, OutSigmoid
import matplotlib.pyplot as plt

from ..xview_base import XviewSegmentationLearner, unwrap_inference_batch, make_img_path_for_inference, save_mask
from ..plotting import tensor_to_img, cmap, bounds, norm

prediction_softmax = OutSoftmax(take_argmax_flag=True)
prediction_sigmoid = OutSigmoid(threshold=0.5)
sigmoid = OutSigmoid(threshold=None)


class E2E_2SegmentationLearner(XviewSegmentationLearner):
    task = 'e2e_2'

    def infer_single_batch(self, batch, device, prediction_type, data_dir):
        x, img_name, dataset = unwrap_inference_batch(batch, device)
        img_name = img_name[0]
        dataset = dataset[0]

        prediction_localization_dir_name = '_'.join([self.model_name, 'localization', prediction_type, str(self.best_model_score)[:6]])
        prediction_damage_dir_name = '_'.join([self.model_name, 'damage', prediction_type, str(self.best_model_score)[:6]])
        img_path_loc = make_img_path_for_inference(data_dir, dataset, prediction_localization_dir_name,
                                                   img_name, 'localization')
        img_path_dmg = make_img_path_for_inference(data_dir, dataset, prediction_damage_dir_name,
                                                   img_name, 'damage')
        if prediction_type is 'prob':
            loc_mask, dmg_mask = self.model.predict_prob(x)

        elif prediction_type is 'mask':
            loc_mask, dmg_mask = self.model.predict(x)
        else:
            raise ValueError('...')

        save_mask(x=loc_mask, path=img_path_loc, prediction_type=prediction_type)
        save_mask(x=dmg_mask, path=img_path_dmg, prediction_type=prediction_type)

    def imshow_batch(self, pr, gt, x):

        x_pre = x[0][0, :, :, :]
        x_post = x[1][0, :, :, :]

        gt_pre, gt_post = gt[0][0, :, :, :],  gt[1][0, :, :, :]
        pr_loc, pr_dmg = pr[0], pr[1]

        pr_loc_mask = prediction_sigmoid(pr_loc)[0, :, :, :]
        pr_dmg_mask = prediction_softmax(pr_dmg)[0, :, :, :]

        x_pre = tensor_to_img(x_pre, mask=False, inverse_preprocess=True)
        x_post = tensor_to_img(x_post, mask=False, inverse_preprocess=True)

        pr_dmg_where_gt = tensor_to_img(pr_dmg_mask * (gt_post > 0).float(), mask=True)

        gt_pre = tensor_to_img(gt_pre, mask=True)
        gt_post = tensor_to_img(gt_post, mask=True)

        pr_loc_mask = tensor_to_img(pr_loc_mask, mask=True)
        pr_dmg_mask = tensor_to_img(pr_dmg_mask, mask=True)

        fig, ax = plt.subplots(4, 2, figsize=(30, 50))

        fig.tight_layout(pad=5)
        im = ax[0, 0].imshow(x_pre); ax[0, 0].set_title('pre', fontsize=30)
        im = ax[0, 1].imshow(x_post); ax[0, 1].set_title('post', fontsize=30)
        im = ax[1, 0].imshow(gt_pre, interpolation='nearest', cmap=cmap, norm=norm); ax[1, 0].set_title('gt_pre', fontsize=30)
        im = ax[1, 1].imshow(gt_post, interpolation='nearest', cmap=cmap, norm=norm); ax[1, 1].set_title('gt_post', fontsize=30)
        im = ax[2, 0].imshow(pr_loc_mask, interpolation='nearest', cmap=cmap, norm=norm); ax[2, 0].set_title('pr_loc', fontsize=30)
        im = ax[2, 1].imshow(pr_dmg_where_gt, interpolation='nearest', cmap=cmap, norm=norm); ax[2, 1].set_title('pr_dmg_where_gt', fontsize=30)
        im = ax[3, 1].imshow(pr_dmg_mask, interpolation='nearest', cmap=cmap, norm=norm); ax[2, 1].set_title('pr_dmg', fontsize=30)

        cbar_ax = fig.add_axes([0.95, 0.32, 0.05, 0.35])
        fig.colorbar(im, cmap=cmap, norm=norm, boundaries=bounds, cax=cbar_ax)
        plt.show()

