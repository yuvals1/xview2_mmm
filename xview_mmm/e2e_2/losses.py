from ..xview_base import to_numpy, to_tensor
from ..xview_base import XviewLoss
import torch
from torch import nn
from torch.nn import functional as F
from .. import functions

reductions_fun = {'harmonic_mean': functions.harmonic_mean, 'arithmetic_mean': functions.arithmetic_mean}


def compute_reg_score(pr):
    return 0.0001 * torch.sum(pr[:, 0, :, :]) + 0.0001 * torch.sum(pr[:, 5, :, :])


class FinalMetricDice(XviewLoss):
    def __init__(self, reduction='harmonic_mean'):
        super().__init__()

        self.softmax2d = torch.nn.Softmax2d()
        self.sigmoid = torch.nn.Sigmoid()
        self.weights_dmg = torch.tensor([0., 1., 1., 1., 1., 0.], device='cuda')
        self.reduction_fun = reductions_fun[reduction]
        self.beta = 1

    def forward(self, pr, gt):

        pr_loc, pr_dmg = pr[0], pr[1]
        gt_loc, gt_dmg = gt[0], gt[1]

        pr_loc = self.sigmoid(pr_loc)
        pr_dmg = self.softmax2d(pr_dmg)

        loc_score = functions.differentiable_f1(pr_loc, gt_loc, self.beta)

        # f_scores_lst = functions.multi_differentiable_f1_mask(pr_dmg * pr_loc, gt_dmg, self.weights_dmg)
        f_scores_lst = functions.multi_differentiable_f1_mask(pr_dmg, gt_dmg, self.weights_dmg)
        dmg_score = self.reduction_fun(f_scores_lst, self.weights_dmg)

        return 1 - 0.3 * loc_score - 0.7 * dmg_score

