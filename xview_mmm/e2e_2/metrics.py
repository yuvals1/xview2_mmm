import numpy as np
import torch

from ..xview_base.metrics import XviewMetric

from segmentation_models_pytorch.layers.out_modules import PredictionLayer
from ..xview_base.metrics import confusion_matrix_compute, recall, precision, f1_score, numpy_harmonic_mean


def loc_score_from_full_cm(cm):
    cm_loc = cm.copy()[:2, :2]
    cm_loc[0, 1] = np.sum(cm.copy()[0, 1:])
    cm_loc[1, 0] = np.sum(cm.copy()[1:, 0])
    cm_loc[1, 1] = np.sum(cm.copy()[1:, 1:])
    loc_score = f1_score(cm_loc, 1)
    return loc_score


def dmg_score_from_full_cm(cm):
    cm_dmg = cm.copy()
    cm_dmg[:, [0, 5]] = 0
    f_scores = []
    for label in [1, 2, 3, 4]:
        f_scores.append(f1_score(cm_dmg, label))
    dmg_score = numpy_harmonic_mean(f_scores)
    return dmg_score


def f1_per_label(cm, label):
    cm_dmg = cm.copy()
    cm_dmg[:, [0, 5]] = 0
    return f1_score(cm_dmg, label)


class MultiTargetConfusionMatrix(XviewMetric):
    def __init__(self):
        super().__init__()
        self.sigmoid = PredictionLayer('sigmoid')
        self.softmax2d = PredictionLayer('softmax2d')
        self.labels = [0, 1, 2, 3, 4, 5]

    def batch_update(self, pr, gt):

        pr_loc, pr_dmg = pr[0], pr[1]
        gt_loc, gt_dmg = gt[0], gt[1]

        pr_loc = self.sigmoid(pr_loc)
        pr_dmg = self.softmax2d(pr_dmg)

        cm_loc = confusion_matrix_compute(pr_loc, gt_loc, self.labels)
        cm_dmg = confusion_matrix_compute(pr_dmg * pr_loc, gt_dmg, self.labels)

        return {'loc': cm_loc, 'dmg': cm_dmg}

    def epoch_update(self, batches_scores_list):

        loc = []
        dmg = []
        for batch_score in batches_scores_list:
            loc.append(batch_score['loc'])
            dmg.append(batch_score['dmg'])

        cm_loc = np.sum(loc, axis=0)
        cm_dmg = np.sum(dmg, axis=0)
        return cm_loc, cm_dmg


class XviewOverallScore(MultiTargetConfusionMatrix):

    def epoch_update(self, batches_scores_list):
        loc = []
        dmg = []
        for batch_score in batches_scores_list:
            loc.append(batch_score['loc'])
            dmg.append(batch_score['dmg'])

        cm_loc = np.sum(loc, axis=0)
        cm_dmg = np.sum(dmg, axis=0)

        loc_score = loc_score_from_full_cm(cm_loc)
        dmg_score = dmg_score_from_full_cm(cm_dmg)
        return 0.3 * loc_score + 0.7 * dmg_score


class LocalizationScore(MultiTargetConfusionMatrix):
    def epoch_update(self, batches_scores_list):
        loc = []
        for batch_score in batches_scores_list:
            loc.append(batch_score['loc'])

        cm_loc = np.sum(loc, axis=0)

        loc_score = loc_score_from_full_cm(cm_loc)
        return loc_score


class DamageScore(MultiTargetConfusionMatrix):
    def epoch_update(self, batches_scores_list):
        dmg = []
        for batch_score in batches_scores_list:
            dmg.append(batch_score['dmg'])

        cm_dmg = np.sum(dmg, axis=0)

        dmg_score = dmg_score_from_full_cm(cm_dmg)
        return dmg_score


class F1_1(MultiTargetConfusionMatrix):
    def epoch_update(self, batches_scores_list):
        dmg = []
        for batch_score in batches_scores_list:
            dmg.append(batch_score['dmg'])
        cm_dmg = np.sum(dmg, axis=0)

        return f1_per_label(cm_dmg, 1)


class F1_2(MultiTargetConfusionMatrix):
    def epoch_update(self, batches_scores_list):
        dmg = []
        for batch_score in batches_scores_list:
            dmg.append(batch_score['dmg'])
        cm_dmg = np.sum(dmg, axis=0)

        return f1_per_label(cm_dmg, 2)


class F1_3(MultiTargetConfusionMatrix):
    def epoch_update(self, batches_scores_list):
        dmg = []
        for batch_score in batches_scores_list:
            dmg.append(batch_score['dmg'])
        cm_dmg = np.sum(dmg, axis=0)

        return f1_per_label(cm_dmg, 3)


class F1_4(MultiTargetConfusionMatrix):
    def epoch_update(self, batches_scores_list):
        dmg = []
        for batch_score in batches_scores_list:
            dmg.append(batch_score['dmg'])
        cm_dmg = np.sum(dmg, axis=0)

        return f1_per_label(cm_dmg, 4)

