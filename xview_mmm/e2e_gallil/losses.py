import cv2
import numpy as np
from ..xview_base import to_numpy, to_tensor
from ..xview_base import XviewLoss
import torch
from torch import nn
from torch.nn import functional as F
from .. import functions
from ..functions import compute_weights_based_mask

reductions_fun = {'harmonic_mean': functions.harmonic_mean, 'arithmetic_mean': functions.arithmetic_mean}


def multi_differentiable_f1_mask(pr, gt, weights, beta=1, eps=1e-4):
    mask = compute_weights_based_mask(gt, weights).float()
    f_scores_lst = []
    for i in range(4):
        gt_i_mask = (gt == i+1).float()
        pr_i = pr[:, i, :, :].unsqueeze(1)
        tp = torch.sum(gt_i_mask * pr_i * mask)
        fp = torch.sum(pr_i * mask) - tp
        fn = torch.sum(gt_i_mask * mask) - tp

        one_class_f_score = ((1 + beta ** 2) * tp + eps) / ((1 + beta ** 2) * tp + beta ** 2 * fn + fp + eps)
        f_scores_lst.append(0.0001 + one_class_f_score)

    return f_scores_lst


class FinalMetricDice(XviewLoss):
    def __init__(self, reduction='harmonic_mean'):
        super().__init__()

        self.softmax2d = torch.nn.Softmax2d()
        self.sigmoid = torch.nn.Sigmoid()
        self.reduction_fun = reductions_fun[reduction]
        self.weights_dmg = torch.tensor([0, 1., 1., 1., 1., 0.], device='cuda')

    def forward(self, pr, gt):

        pr0 = self.sigmoid(pr[:, 0:1, :, :])
        pr1_5 = self.softmax2d(pr[:, 1:5, :, :])
        gt_0_mask = (gt != 0).float()
        loc_score = functions.differentiable_f1(pr0, gt_0_mask)

        f_scores_lst = multi_differentiable_f1_mask(pr1_5, gt, self.weights_dmg)
        # f_scores_lst = multi_differentiable_f1_mask(pr0 * pr1_5, gt, self.weights_dmg)
        dmg_score = self.reduction_fun(f_scores_lst, self.weights_dmg[1:5])

        return 1 - 0.3 * loc_score - 0.7 * dmg_score


class FinalMetricDiceFocal(XviewLoss):
    def __init__(self, reduction='harmonic_mean', alpha=1):
        super().__init__()

        self.softmax2d = torch.nn.Softmax2d()
        self.weights_dmg = torch.tensor([0, 1., 1., 1., 1., 0.], device='cuda')
        self.reduction_fun = reductions_fun[reduction]
        self.gamma = 1
        self.alpha = alpha

    def forward(self, pr, gt):
        pr = self.softmax2d(pr)
        pr_0 = pr[:, 0, :, :].unsqueeze(1)
        gt_0_mask = (gt == 0).float()
        loc_score = functions.differentiable_f1(1 - pr_0, 1 - gt_0_mask)

        f_scores_lst = functions.focal_multi_differentiable_f1_mask(pr, gt, self.weights_dmg, gamma=self.gamma)
        dmg_score = self.reduction_fun(f_scores_lst, self.weights_dmg)

        h, w = gt.shape[-2], gt.shape[-1]
        gt_new = gt.view(-1, h, w).type(torch.cuda.LongTensor)

        logpt = -F.cross_entropy(pr, gt_new, weight=torch.tensor([1., 1., 1., 1., 1., 0.], device='cuda'), reduction='none')
        pt = torch.exp(logpt)
        focal_mask = (1 - pt).pow(self.gamma)
        class_weights_mask = 0
        for i in range(5):
            class_weights_mask += (gt_new == i).float()

        focal_mask = class_weights_mask * focal_mask
        focal_mat = focal_mask * (-logpt)
        focal_loss = focal_mat.mean()

        return 1 - 0.3 * loc_score - 0.7 * dmg_score + self.alpha * focal_loss


class FinalMetricDiceWeight(XviewLoss):
    def __init__(self, reduction='harmonic_mean', loc_weight=0.3, dmg_weight=0.7):
        super().__init__()

        self.softmax2d = torch.nn.Softmax2d()
        self.weights_dmg = torch.tensor([0, 1., 1., 1., 1., 0.], device='cuda')
        self.reduction_fun = reductions_fun[reduction]
        self.dmg_weight = dmg_weight
        self.loc_weight = loc_weight
        self.gamma = 1

    def forward(self, pr, gt):
        pr = self.softmax2d(pr)
        pr_0 = pr[:, 0, :, :].unsqueeze(1)
        gt_0_mask = (gt == 0).float()
        loc_score = functions.differentiable_f1(1 - pr_0, 1 - gt_0_mask)

        f_scores_lst = functions.multi_differentiable_f1_mask(pr, gt, self.weights_dmg)
        dmg_score = self.reduction_fun(f_scores_lst, self.weights_dmg)

        return 1 - self.loc_weight * loc_score - self.dmg_weight * dmg_score


class FinalMetricFocal(XviewLoss):
    def __init__(self, reduction='harmonic_mean'):
        super().__init__()

        self.softmax2d = torch.nn.Softmax2d()
        self.weights_dmg = torch.tensor([0, 1., 1., 1., 1., 0.], device='cuda')
        self.reduction_fun = reductions_fun[reduction]
        self.gamma = 1

    def forward(self, pr, gt):
        h, w = gt.shape[-2], gt.shape[-1]
        gt = gt.view(-1, h, w).type(torch.cuda.LongTensor)

        logpt = -F.cross_entropy(pr, gt, weight=torch.tensor([1., 1., 1., 1., 1., 0.], device='cuda'), reduction='none')
        pt = torch.exp(logpt)
        focal_mask = (1 - pt).pow(self.gamma)
        class_weights_mask = 0
        for i in range(5):
            class_weights_mask += (gt == i).float()

        focal_mask = class_weights_mask * focal_mask
        focal_mat = focal_mask * (-logpt)
        focal_loss = focal_mat.mean()

        return focal_loss


def make_single_contour_mask(gt):
    h, w = gt.shape
    a = np.zeros((h, w))
    contours, _ = cv2.findContours(gt, cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)

    for c in contours:
        cv2.drawContours(a, c, -1, 1, 3)

    a = 2 * cv2.blur(a, (19, 19))
    return a


def get_contours_mask(gt):

    b = gt.shape[0]
    tensors = []
    # gt_ = (gt > 0).type(torch.cuda.FloatTensor)
    for i in range(b):
        numpy_mask = to_numpy(gt[i, :, :, :].detach().cpu()).astype(float)
        contour_mask_numpy = make_single_contour_mask(numpy_mask[:, :, 0].astype('uint8'))
        contour_mask = to_tensor(contour_mask_numpy).cuda()
        tensors.append(contour_mask)

    return torch.stack(tensors, dim=0)


class FinalMetricContour(XviewLoss):
    def __init__(self, reduction='harmonic_mean', alpha=20):
        super().__init__()

        self.softmax2d = torch.nn.Softmax2d()
        self.weights_dmg = torch.tensor([0, 1., 1., 1., 1., 0.], device='cuda')
        self.reduction_fun = reductions_fun[reduction]
        self.gamma = 1
        self.alpha = alpha

    def forward(self, pr, gt):
        pr = self.softmax2d(pr)
        pr_0 = pr[:, 0, :, :].unsqueeze(1)
        gt_0_mask = (gt == 0).float()
        loc_score = functions.differentiable_f1(1 - pr_0, 1 - gt_0_mask)

        f_scores_lst = functions.focal_multi_differentiable_f1_mask(pr, gt, self.weights_dmg, gamma=self.gamma)
        dmg_score = self.reduction_fun(f_scores_lst, self.weights_dmg)

        h, w = gt.shape[-2], gt.shape[-1]

        contour_mask = get_contours_mask(gt)
        gt = gt.view(-1, h, w).type(torch.cuda.LongTensor)

        logpt = -F.cross_entropy(pr, gt, weight=torch.tensor([1., 1., 1., 1., 1., 0.], device='cuda'), reduction='none')

        class_weights_mask = 0
        for i in range(5):
            class_weights_mask += (gt == i).float()

        contour_mask = class_weights_mask * contour_mask
        loss_mat = contour_mask * (-logpt)
        bce_contour_loss = loss_mat.mean()

        return 1 - 0.3 * loc_score - 0.7 * dmg_score + self.alpha * bce_contour_loss

