import numpy as np
from ..xview_base.metrics import XviewMetric, f1_score, numpy_harmonic_mean, recall, precision
from segmentation_models_pytorch.layers.out_modules import PredictionLayer


def confusion_matrix_compute(pr, gt, labels):
    num_labels = len(labels)
    cm = np.zeros((num_labels, num_labels))
    for i, label_i in enumerate(labels):
        for j, label_j in enumerate(labels):
            if type(pr) == np.ndarray:
                cm[i, j] = ((pr == label_i) & (gt == label_j)).sum()
            else:
                cm[i, j] = ((pr == label_i) & (gt == label_j)).sum().float().cpu().numpy()
    return cm


class ConfusionMatrix(XviewMetric):
    def __init__(self, activation, labels, chosen_label=None, pre_post=False):
        super().__init__()
        self.sigmoid = PredictionLayer('sigmoid')
        self.softmax = PredictionLayer('softmax2d')
        self.activation = activation
        self.labels = labels
        self.chosen_label = chosen_label
        self.pre_post = pre_post

    def batch_update(self, pr, gt):

        pr0 = self.sigmoid(pr[:, 0:1, :, :])
        pr1_5 = self.softmax(pr[:, 1:5, :, :]) + 1
        pr = pr0 * pr1_5

        cm = confusion_matrix_compute(pr, gt, self.labels)
        return cm

    def epoch_update(self, batches_scores_list):
        cm = np.sum(batches_scores_list, axis=0)
        return cm


def loc_score_from_full_cm(cm):
    cm_loc = cm.copy()[:2, :2]
    cm_loc[0, 1] = np.sum(cm.copy()[0, 1:])
    cm_loc[1, 0] = np.sum(cm.copy()[1:, 0])
    cm_loc[1, 1] = np.sum(cm.copy()[1:, 1:])
    loc_score = f1_score(cm_loc, 1)
    return loc_score


def dmg_score_from_full_cm(cm):
    cm_dmg = cm.copy()
    cm_dmg[:, [0, 5]] = 0
    f_scores = []
    for label in [1, 2, 3, 4]:
        f_scores.append(f1_score(cm_dmg, label))
    dmg_score = numpy_harmonic_mean(f_scores)
    return dmg_score


def loc_precision_from_full_cm(cm):
    cm_loc = cm.copy()[:2, :2]
    cm_loc[0, 1] = np.sum(cm.copy()[0, 1:])
    cm_loc[1, 0] = np.sum(cm.copy()[1:, 0])
    cm_loc[1, 1] = np.sum(cm.copy()[1:, 1:])
    loc_score = precision(cm_loc, 1)
    return loc_score


def loc_recall_from_full_cm(cm):
    cm_loc = cm.copy()[:2, :2]
    cm_loc[0, 1] = np.sum(cm.copy()[0, 1:])
    cm_loc[1, 0] = np.sum(cm.copy()[1:, 0])
    cm_loc[1, 1] = np.sum(cm.copy()[1:, 1:])
    loc_score = recall(cm_loc, 1)
    return loc_score


def f1_per_label(cm, label):
    cm_dmg = cm.copy()
    cm_dmg[:, [0, 5]] = 0
    return f1_score(cm_dmg, label)


def precision_per_label(cm, label):
    cm_dmg = cm.copy()
    cm_dmg[:, [0, 5]] = 0
    return precision(cm_dmg, label)


def recall_per_label(cm, label):
    cm_dmg = cm.copy()
    cm_dmg[:, [0, 5]] = 0
    return recall(cm_dmg, label)


class E2EMetric(ConfusionMatrix):
    def __init__(self, activation='softmax2d', pre_post=False):
        super().__init__(activation, [0, 1, 2, 3, 4, 5], pre_post=pre_post)


class XviewScorePost(E2EMetric):
    def epoch_update(self, batches_scores_list):
        cm = np.sum(batches_scores_list, axis=0)
        loc_score = loc_score_from_full_cm(cm)
        dmg_score = dmg_score_from_full_cm(cm)
        return 0.3 * loc_score + 0.7 * dmg_score


class LocalizationScore(E2EMetric):
    def epoch_update(self, batches_scores_list):
        cm = np.sum(batches_scores_list, axis=0)
        loc_score = loc_score_from_full_cm(cm)
        return loc_score


class DamageScore(E2EMetric):
    def epoch_update(self, batches_scores_list):
        cm = np.sum(batches_scores_list, axis=0)
        dmg_score = dmg_score_from_full_cm(cm)
        return dmg_score


class LocalizationPrecision(E2EMetric):
    def epoch_update(self, batches_scores_list):
        cm = np.sum(batches_scores_list, axis=0)
        loc_score = loc_precision_from_full_cm(cm)
        return loc_score


class LocalizationRecall(E2EMetric):
    def epoch_update(self, batches_scores_list):
        cm = np.sum(batches_scores_list, axis=0)
        loc_score = loc_recall_from_full_cm(cm)
        return loc_score


class F1_1(E2EMetric):
    def epoch_update(self, batches_scores_list):
        cm = np.sum(batches_scores_list, axis=0)
        return f1_per_label(cm, 1)


class F1_2(E2EMetric):
    def epoch_update(self, batches_scores_list):
        cm = np.sum(batches_scores_list, axis=0)
        return f1_per_label(cm, 2)


class F1_3(E2EMetric):
    def epoch_update(self, batches_scores_list):
        cm = np.sum(batches_scores_list, axis=0)
        return f1_per_label(cm, 3)


class F1_4(E2EMetric):
    def epoch_update(self, batches_scores_list):
        cm = np.sum(batches_scores_list, axis=0)
        return f1_per_label(cm, 4)


class Precision1(E2EMetric):
    def epoch_update(self, batches_scores_list):
        cm = np.sum(batches_scores_list, axis=0)
        return precision_per_label(cm, 1)


class Precision2(E2EMetric):
    def epoch_update(self, batches_scores_list):
        cm = np.sum(batches_scores_list, axis=0)
        return precision_per_label(cm, 2)


class Precision3(E2EMetric):
    def epoch_update(self, batches_scores_list):
        cm = np.sum(batches_scores_list, axis=0)
        return precision_per_label(cm, 3)


class Precision4(E2EMetric):
    def epoch_update(self, batches_scores_list):
        cm = np.sum(batches_scores_list, axis=0)
        return precision_per_label(cm, 4)


class Recall1(E2EMetric):
    def epoch_update(self, batches_scores_list):
        cm = np.sum(batches_scores_list, axis=0)
        return recall_per_label(cm, 1)


class Recall2(E2EMetric):
    def epoch_update(self, batches_scores_list):
        cm = np.sum(batches_scores_list, axis=0)
        return recall_per_label(cm, 2)


class Recall3(E2EMetric):
    def epoch_update(self, batches_scores_list):
        cm = np.sum(batches_scores_list, axis=0)
        return recall_per_label(cm, 3)


class Recall4(E2EMetric):
    def epoch_update(self, batches_scores_list):
        cm = np.sum(batches_scores_list, axis=0)
        return recall_per_label(cm, 4)



