import torch
from segmentation_models_pytorch.layers.out_modules import OutSoftmax, OutSigmoid
import matplotlib.pyplot as plt

from ..xview_base import XviewSegmentationLearner, unwrap_inference_batch, make_img_path_for_inference, save_mask
from ..plotting import tensor_to_img, cmap, bounds, norm

prediction_softmax = OutSoftmax(take_argmax_flag=True)
prediction_sigmoid = OutSigmoid(threshold=0.5)
sigmoid = OutSigmoid(threshold=None)


class E2ESegmentationLearner(XviewSegmentationLearner):
    task = 'e2e'

    def infer_single_batch(self, batch, device, prediction_type, data_dir):
        x, img_name, dataset = unwrap_inference_batch(batch, device)
        img_name = img_name[0]
        dataset = dataset[0]

        prediction_localization_dir_name = '_'.join([self.model_name, 'localization', prediction_type, str(self.best_model_score)[:6]])
        prediction_damage_dir_name = '_'.join([self.model_name, 'damage', prediction_type, str(self.best_model_score)[:6]])
        img_path_loc = make_img_path_for_inference(data_dir, dataset, prediction_localization_dir_name,
                                                   img_name, 'localization')
        img_path_dmg = make_img_path_for_inference(data_dir, dataset, prediction_damage_dir_name,
                                                   img_name, 'damage')
        if prediction_type is 'prob':
            x = self.model.predict_prob(x)

        elif prediction_type is 'mask':
            x = self.model.predict(x)
        else:
            raise ValueError('...')

        dmg_mask = x
        save_mask(x=dmg_mask, path=img_path_dmg, prediction_type=prediction_type)

        loc_mask = (x > 0).float()
        save_mask(x=loc_mask, path=img_path_loc, prediction_type=prediction_type)

    def imshow_batch(self, pr, gt, x):

        pr = pr[0]
        gt = gt[0]

        x_pre_0 = x[0][0, :, :, :]
        x_post_0 = x[1][0, :, :, :]
        gt_0 = gt[0, :, :, :]
        pr_0 = prediction_softmax(pr)[0, :, :, :]

        x_pre_img = tensor_to_img(x_pre_0, mask=False, inverse_preprocess=True)
        x_post_img = tensor_to_img(x_post_0, mask=False, inverse_preprocess=True)

        gt_img = tensor_to_img(gt_0, mask=True)
        pr_img = tensor_to_img(pr_0, mask=True)

        fig, ax = plt.subplots(2, 2, figsize=(14, 14))

        fig.tight_layout(pad=5)
        im = ax[0, 0].imshow(x_pre_img);
        ax[0, 0].set_title('pre', fontsize=30)
        im = ax[0, 1].imshow(x_post_img);
        ax[0, 1].set_title('post', fontsize=30)
        im = ax[1, 0].imshow(gt_img, interpolation='nearest', cmap=cmap, norm=norm);
        ax[1, 0].set_title('gt', fontsize=30)
        im = ax[1, 1].imshow(pr_img, interpolation='nearest', cmap=cmap, norm=norm);
        ax[1, 1].set_title('pr', fontsize=30)

        cbar_ax = fig.add_axes([0.95, 0.32, 0.05, 0.35])
        fig.colorbar(im, cmap=cmap, norm=norm, boundaries=bounds, cax=cbar_ax)
        plt.show()

