from ..xview_base import to_numpy, to_tensor
from ..xview_base import XviewLoss
import torch
from torch import nn
from torch.nn import functional as F
from .. import functions
from ..e2e.losses import FinalMetricDice


class FinalMetricDiceMtNew(XviewLoss):
    def __init__(self):
        super().__init__()

        self.final_metric_loss = FinalMetricDice()
        self.mse = nn.MSELoss()

    def forward(self, pr, gt):
        pr, pr4 = pr[0], pr[1]

        d = []
        for i in range(1, 5):
            d.append((gt == i).float().sum(dim=3).sum(dim=2).sum(dim=1) / pr.shape[3]**2)
        classes_dist = torch.stack(d).transpose(0, 1)

        final_loss_score = self.final_metric_loss(pr, gt)
        mse_loss = self.mse(pr4, classes_dist)

        return 0.7 * final_loss_score + 0.3 * mse_loss


class FinalMetricDiceMt(XviewLoss):
    def __init__(self):
        super().__init__()

        self.final_metric_loss = FinalMetricDice()
        self.ce = nn.CrossEntropyLoss()

    def forward(self, pr, gt):

        pr_mask, pr_disaster = pr[0], pr[1]
        gt_mask, gt_disaster = gt[0], gt[1]

        final_loss_score = self.final_metric_loss(pr_mask, gt_mask)
        disaster_score = self.ce(pr_disaster, gt_disaster)

        return 0.7 * final_loss_score + 0.3 * disaster_score


class FinalMetricDiceOnly(FinalMetricDiceMt):

    def forward(self, pr, gt):
        pr_mask, pr_disaster = pr[0], pr[1]
        gt_mask, gt_disaster = gt[0], gt[1]

        final_loss_score = self.final_metric_loss(pr_mask, gt_mask)

        return final_loss_score


class DisasterLoss(FinalMetricDiceMt):

    def forward(self, pr, gt):
        pr_mask, pr_disaster = pr[0], pr[1]
        gt_mask, gt_disaster = gt[0], gt[1]

        disaster_score = self.ce(pr_disaster, gt_disaster)

        return disaster_score

