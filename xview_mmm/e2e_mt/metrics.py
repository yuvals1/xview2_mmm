from segmentation_models_pytorch.layers.out_modules import PredictionLayer
import numpy as np
from ai_learner.ann import Callback

from ..xview_base.metrics import confusion_matrix_compute
from ..xview_base import XviewMetric
from ..xview_base.metrics import ConfusionMatrix, f1_score, numpy_harmonic_mean, recall, precision


class ConfusionMatrixMT0(XviewMetric):
    def __init__(self):
        super().__init__()
        self.softmax = PredictionLayer('softmax2d')
        self.labels = [0, 1, 2, 3, 4, 5]

    def batch_update(self, pr, gt):

        pr_mask, pr_disaster = pr[0], pr[1]
        gt_mask, gt_disaster = gt[0], gt[1]

        pr_mask = self.softmax(pr_mask)
        cm = confusion_matrix_compute(pr_mask, gt_mask, self.labels)
        return cm

    def epoch_update(self, batches_scores_list):
        cm = np.sum(batches_scores_list, axis=0)
        return cm


def loc_score_from_full_cm(cm):
    cm_loc = cm.copy()[:2, :2]
    cm_loc[0, 1] = np.sum(cm.copy()[0, 1:])
    cm_loc[1, 0] = np.sum(cm.copy()[1:, 0])
    cm_loc[1, 1] = np.sum(cm.copy()[1:, 1:])
    loc_score = f1_score(cm_loc, 1)
    return loc_score


def dmg_score_from_full_cm(cm):
    cm_dmg = cm.copy()
    cm_dmg[:, [0, 5]] = 0
    f_scores = []
    for label in [1, 2, 3, 4]:
        f_scores.append(f1_score(cm_dmg, label))
    dmg_score = numpy_harmonic_mean(f_scores)
    return dmg_score


def loc_precision_from_full_cm(cm):
    cm_loc = cm.copy()[:2, :2]
    cm_loc[0, 1] = np.sum(cm.copy()[0, 1:])
    cm_loc[1, 0] = np.sum(cm.copy()[1:, 0])
    cm_loc[1, 1] = np.sum(cm.copy()[1:, 1:])
    loc_score = precision(cm_loc, 1)
    return loc_score


def loc_recall_from_full_cm(cm):
    cm_loc = cm.copy()[:2, :2]
    cm_loc[0, 1] = np.sum(cm.copy()[0, 1:])
    cm_loc[1, 0] = np.sum(cm.copy()[1:, 0])
    cm_loc[1, 1] = np.sum(cm.copy()[1:, 1:])
    loc_score = recall(cm_loc, 1)
    return loc_score


def f1_per_label(cm, label):
    cm_dmg = cm.copy()
    cm_dmg[:, [0, 5]] = 0
    return f1_score(cm_dmg, label)


def precision_per_label(cm, label):
    cm_dmg = cm.copy()
    cm_dmg[:, [0, 5]] = 0
    return precision(cm_dmg, label)


def recall_per_label(cm, label):
    cm_dmg = cm.copy()
    cm_dmg[:, [0, 5]] = 0
    return recall(cm_dmg, label)


class XviewScorePost(ConfusionMatrixMT0):
    def epoch_update(self, batches_scores_list):
        cm = np.sum(batches_scores_list, axis=0)
        loc_score = loc_score_from_full_cm(cm)
        dmg_score = dmg_score_from_full_cm(cm)
        return 0.3 * loc_score + 0.7 * dmg_score


class LocalizationScore(ConfusionMatrixMT0):
    def epoch_update(self, batches_scores_list):
        cm = np.sum(batches_scores_list, axis=0)
        loc_score = loc_score_from_full_cm(cm)
        return loc_score


class DamageScore(ConfusionMatrixMT0):
    def epoch_update(self, batches_scores_list):
        cm = np.sum(batches_scores_list, axis=0)
        dmg_score = dmg_score_from_full_cm(cm)
        return dmg_score


class LocalizationPrecision(ConfusionMatrixMT0):
    def epoch_update(self, batches_scores_list):
        cm = np.sum(batches_scores_list, axis=0)
        loc_score = loc_precision_from_full_cm(cm)
        return loc_score


class LocalizationRecall(ConfusionMatrixMT0):
    def epoch_update(self, batches_scores_list):
        cm = np.sum(batches_scores_list, axis=0)
        loc_score = loc_recall_from_full_cm(cm)
        return loc_score


class F1_1(ConfusionMatrixMT0):
    def epoch_update(self, batches_scores_list):
        cm = np.sum(batches_scores_list, axis=0)
        return f1_per_label(cm, 1)


class F1_2(ConfusionMatrixMT0):
    def epoch_update(self, batches_scores_list):
        cm = np.sum(batches_scores_list, axis=0)
        return f1_per_label(cm, 2)


class F1_3(ConfusionMatrixMT0):
    def epoch_update(self, batches_scores_list):
        cm = np.sum(batches_scores_list, axis=0)
        return f1_per_label(cm, 3)


class F1_4(ConfusionMatrixMT0):
    def epoch_update(self, batches_scores_list):
        cm = np.sum(batches_scores_list, axis=0)
        return f1_per_label(cm, 4)


class Precision1(ConfusionMatrixMT0):
    def epoch_update(self, batches_scores_list):
        cm = np.sum(batches_scores_list, axis=0)
        return precision_per_label(cm, 1)


class Precision2(ConfusionMatrixMT0):
    def epoch_update(self, batches_scores_list):
        cm = np.sum(batches_scores_list, axis=0)
        return precision_per_label(cm, 2)


class Precision3(ConfusionMatrixMT0):
    def epoch_update(self, batches_scores_list):
        cm = np.sum(batches_scores_list, axis=0)
        return precision_per_label(cm, 3)


class Precision4(ConfusionMatrixMT0):
    def epoch_update(self, batches_scores_list):
        cm = np.sum(batches_scores_list, axis=0)
        return precision_per_label(cm, 4)


class Recall1(ConfusionMatrixMT0):
    def epoch_update(self, batches_scores_list):
        cm = np.sum(batches_scores_list, axis=0)
        return recall_per_label(cm, 1)


class Recall2(ConfusionMatrixMT0):
    def epoch_update(self, batches_scores_list):
        cm = np.sum(batches_scores_list, axis=0)
        return recall_per_label(cm, 2)


class Recall3(ConfusionMatrixMT0):
    def epoch_update(self, batches_scores_list):
        cm = np.sum(batches_scores_list, axis=0)
        return recall_per_label(cm, 3)


class Recall4(ConfusionMatrixMT0):
    def epoch_update(self, batches_scores_list):
        cm = np.sum(batches_scores_list, axis=0)
        return recall_per_label(cm, 4)


class ConfusionMatrixMT1(XviewMetric):
    def __init__(self):
        super().__init__()
        self.softmax = PredictionLayer('softmax')
        self.labels = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

    def batch_update(self, pr, gt):
        pr_mask, pr_disaster = pr[0], pr[1]
        gt_mask, gt_disaster = gt[0], gt[1]

        pr_disaster = self.softmax(pr_disaster)
        cm = confusion_matrix_compute(pr_disaster, gt_disaster, self.labels)
        return cm

    def epoch_update(self, batches_scores_list):
        cm = np.sum(batches_scores_list, axis=0)
        return cm


class ConfusionMatrixCBMT(Callback):
    def __init__(self):
        self.CM1 = ConfusionMatrixMT0()
        self.CM2 = ConfusionMatrixMT1()
        self.cm_train = 0
        self.cm_train_dis = 0
        self.cm_valid = 0
        self.cm_valid_dis = 0

    def batch_ended(self, learner, phase, pr, gt, x, **kwargs):
        if phase.name == 'training':
            self.cm_train += self.CM1(pr, gt)
            self.cm_train_dis += self.CM2(pr, gt)
        elif phase.name == 'validation':
            self.cm_valid += self.CM1(pr, gt)
            self.cm_valid_dis += self.CM2(pr, gt)

    def epoch_ended(self, learner, **kwargs):
        cm_train = np.round(self.cm_train / np.sum(self.cm_train), 3)
        cm_valid = np.round(self.cm_valid / np.sum(self.cm_valid), 3)
        cm_train_dis = np.round(self.cm_train_dis / np.sum(self.cm_train_dis), 4)
        cm_valid_dis = np.round(self.cm_valid_dis / np.sum(self.cm_valid_dis), 4)
        print('training: ')
        print(cm_train)
        print(cm_train_dis)
        print('validation: ')
        print(cm_valid)
        print(cm_valid_dis)
