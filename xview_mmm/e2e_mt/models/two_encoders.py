from typing import Optional, Union, List
import torch
from segmentation_models_pytorch import Unet, SegmentationModel, SegmentationHead, \
    ClassificationHead, get_encoder
from segmentation_models_pytorch.unet.decoder import UnetDecoder


class TwoEncodersUnet(SegmentationModel):
    def __init__(
        self,
        encoder_name: str = "resnet34",
        encoder_depth: int = 5,
        encoder_weights: str = "imagenet",
        decoder_use_batchnorm: bool = True,
        decoder_channels: List[int] = (256, 128, 64, 32, 16),
        decoder_attention_type: Optional[str] = None,
        activation: Optional[Union[str, callable]] = None,
        aux_params: Optional[dict] = {},
        segmentation_head_kernel_size: int = 3,
        num_buildings_task=True,
        num_buildings_pixels=True,


    ):
        num_classes = 6
        in_channels = 3
        super().__init__(num_classes=num_classes)

        self.encoder1 = get_encoder(
            encoder_name,
            in_channels=in_channels,
            depth=encoder_depth,
            weights=encoder_weights,
        )

        self.encoder2 = get_encoder(
            encoder_name,
            in_channels=in_channels,
            depth=encoder_depth,
            weights=encoder_weights,
        )

        encoders_out_channels = tuple([x + y for x, y in zip(self.encoder1.out_channels, self.encoder2.out_channels)])
        self.decoder = UnetDecoder(
            encoder_channels=encoders_out_channels,
            decoder_channels=decoder_channels,
            n_blocks=encoder_depth,
            use_batchnorm=decoder_use_batchnorm,
            center=True if encoder_name.startswith("vgg") else False,
            attention_type=decoder_attention_type,
        )

        self.segmentation_head = SegmentationHead(
            in_channels=decoder_channels[-1],
            out_channels=num_classes,
            activation=activation,
            kernel_size=segmentation_head_kernel_size,
        )

        if aux_params is not None:
            print(1)
            self.classification_head = ClassificationHead(
                in_channels=encoders_out_channels[-1], classes=4,
            )
        else:
            self.classification_head = None

        self.name = "u-{}".format(encoder_name)

    def forward(self, x):
        """Sequentially pass `x` trough model`s encoder, decoder and heads"""
        pre = x[0]
        post = x[1]
        pre = self.encoder1(pre)
        post = self.encoder2(post)
        concat_features = [torch.cat([feature1, feature2], dim=1) for feature1, feature2 in zip(pre, post)]
        decoder_output = self.decoder(*concat_features)

        masks = self.segmentation_head(decoder_output)

        if self.classification_head is not None:
            labels = self.classification_head(concat_features[-1])
            return masks, labels

        return masks
