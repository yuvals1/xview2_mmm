import torch

from ..xview_base import XviewLoss
from .. import functions
from ..localization import losses as loc_losses
from ..damage import losses as dmg_losses

reductions_fun = {'harmonic_mean': functions.harmonic_mean, 'arithmetic_mean': functions.arithmetic_mean}


class Dice(XviewLoss):
    def __init__(self, weights, beta=1, reduction='harmonic_mean'):
        super().__init__()
        self.loc_dice = loc_losses.Dice(beta)
        self.dmg_dice = dmg_losses.Dice(weights, reduction=reduction)

    def forward(self, pr, gt):
        pr_loc, pr_dmg = pr[0], pr[1]
        gt_loc, gt_dmg = gt[0], gt[1]

        loc_score = -self.loc_dice(pr_loc, pr_dmg) + 1
        dmg_score = -self.dmg_dice(pr_loc, pr_dmg) + 1

        return 1 - 0.3 * loc_score - 0.7 * dmg_score


class DiceWithRegression(lsl.Loss):
    def __init__(self):
        super().__init__()
        self.__name__ = '_'.join([to_snake_case(get_class_name(self))])
        self.softmax2d = torch.nn.Softmax2d()


class XviewMultiTargetCE(lsl.Loss):
    def __init__(self, weights):
        super().__init__()
        self.__name__ = '_'.join([to_snake_case(get_class_name(self))])

        self.loc_loss = lsl.Dice()
        self.dmg_loss = dcl.CE(weights)

    def forward(self, pr, gt):
        pr_local, pr_damage = pr[0], pr[1]
        gt_local, gt_damage = gt[0], gt[1]
        local_loss = self.loc_loss(pr_local, gt_local)
        damage_loss = self.dmg_loss(pr_damage, gt_damage)
        return 4 * local_loss + damage_loss


class XviewDamage(lsl.Loss):
    def __init__(self, weights):
        super().__init__()
        self.__name__ = '_'.join([to_snake_case(get_class_name(self))])

        self.loc_loss = lsl.BCE()
        self.dmg_loss = dcl.CE(weights)

    def forward(self, pr, gt):
        pr_local, pr_damage = pr[0], pr[1]
        gt_local, gt_damage = gt[0], gt[1]
        damage_loss = self.dmg_loss(pr_damage, gt_damage)
        return damage_loss


class XviewLocalization(lsl.Loss):
    def __init__(self, weights):
        super().__init__()
        self.__name__ = '_'.join([to_snake_case(get_class_name(self))])

        self.loc_loss = lsl.BCE()
        self.dmg_loss = dcl.CE(weights)

    def forward(self, pr, gt):
        pr_local, pr_damage = pr[0], pr[1]
        gt_local, gt_damage = gt[0], gt[1]
        local_loss = self.loc_loss(pr_local, gt_local)
        return local_loss


class MinusXviewMultiTargetCE(lsl.Loss):
    def __init__(self, weights):
        super().__init__()
        self.__name__ = '_'.join([to_snake_case(get_class_name(self))])

        self.loss = XviewMultiTargetCE(weights)

    def forward(self, pr, gt):
        return -self.loss(pr, gt)


class TempXviewLocalization(em.EpochMetric):
    def __init__(self):
        super().__init__()
        self.__name__ = to_snake_case(get_class_name(self))
        self.f1_score = em.F1Score(1, 'sigmoid')

    def update_epoch_metric(self, batches_scores):
        return self.f1_score.update_epoch_metric(batches_scores)

    def forward(self, pr, gt):
        pr_local, pr_damage = pr[0], pr[1]
        gt_local, gt_damage = gt[0], gt[1]
        return self.f1_score(pr_local, gt_local)


class TempXviewDamage(em.EpochMetric):
    def __init__(self):
        super().__init__()
        self.__name__ = to_snake_case(get_class_name(self))
        self.harmonic_mean_f1 = em.HarmonicMeanF1('softmax2d', weights=torch.tensor([0., 1., 1., 1., 1., 0.],
                                                                                    device='cuda'))

    def update_epoch_metric(self, batches_scores):
        return self.harmonic_mean_f1.update_epoch_metric(batches_scores)

    def forward(self, pr, gt):
        pr_local, pr_damage = pr[0], pr[1]
        gt_local, gt_damage = gt[0], gt[1]
        return self.harmonic_mean_f1(pr_damage, gt_damage)
