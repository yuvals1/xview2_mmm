from xview_mmm.xview_base.metrics.confusion_matrix import TwoOutputsScore
from ..localization import LocalizationScore
from ..damage import DamageScore


class OverallScore(TwoOutputsScore):
    def __init__(self):
        super().__init__(class_a=LocalizationScore, class_b=DamageScore, weight_a=0.3, weight_b=0.7)


