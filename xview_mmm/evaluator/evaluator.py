import multiprocessing
from torch.utils.data import DataLoader
import pandas as pd

from ..xview_base import MatchingFoldersDataset, to_device, XviewMetricsHolder

num_workers = multiprocessing.cpu_count() - 1


class Evaluator:
    def __init__(self, metrics, pr_folder_name, gt_folder_name, root_dir, img_names=None):

        self.dataset = MatchingFoldersDataset(*[pr_folder_name, gt_folder_name], root_dir_path=root_dir,
                                              img_names=img_names)
        # self.loader = DataLoader(self.dataset, batch_size=1, shuffle=False, num_workers=num_workers)
        self.img_names = img_names
        self.metrics = metrics
        self.metrics_holder = XviewMetricsHolder(metrics)

    def run(self):

        for batch, img_name in self.dataset:
            pr, gt = batch
            self.metrics_holder.batch_ended_update(pr, gt)

        self.metrics_holder.create_batches_df()
        if self.img_names:
            self.metrics_holder.cm_df['img_name'] = self.img_names
        self.metrics_holder.epoch_ended_update()
        return self.metrics_holder


# class Evaluator:
#     def __init__(self, metrics, pr_folder_name, gt_folder_name, root_dir, img_names=None, device='cuda'):
#
#         self.dataset = MatchingFoldersDataset(*[pr_folder_name, gt_folder_name], root_dir_path=root_dir,
#                                               img_names=img_names)
#         self.loader = DataLoader(self.dataset, batch_size=1, shuffle=False, num_workers=num_workers)
#         self.img_names = img_names
#         self.device = device
#         self.metrics = metrics
#         self.metrics_holder = XviewMetricsHolder(metrics)
#
#     def run(self):
#
#         for batch, img_name in self.loader:
#             img_name = img_name[0]
#             pr, gt = to_device(batch, self.device)
#             self.metrics_holder.batch_ended_update(pr, gt)
#
#         scores_by_metric = {metric_name: [] for metric_name in self.metrics_names}
#
#         for metric_name in self.metrics_holder.metrics_names:
#             images_sorted_by_metric[metric_name]['score'] = self.metrics_holder.batches_history[metric_name].copy()
#
#         self.metrics_holder.epoch_ended_update()
