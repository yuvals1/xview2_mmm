import torch


def confusion_measures(pr, gt, chosen_class=1):
    """"
    get gt which contains {0,1} and pr that contains values in (0,1)
    1 - positive
    0 - negative
    return tp, fp, fn, tn
    """

    tp = ((pr == chosen_class) & (gt == chosen_class)).sum().float()
    fp = ((pr == chosen_class) & (gt != chosen_class)).sum().float()
    fn = ((pr != chosen_class) & (gt == chosen_class)).sum().float()
    tn = ((pr != chosen_class) & (gt != chosen_class)).sum().float()

    return tp, fp, fn, tn


def masked_confusion_measures(pr, gt, chosen_class, mask):
    """"
    get gt which contains {0,1} and pr that contains values in (0,1)
    1 - positive
    0 - negative
    return tp, fp, fn, tn
    """
    tp = ((pr == chosen_class) & (gt == chosen_class) & mask).sum().float()
    fp = ((pr == chosen_class) & (gt != chosen_class) & mask).sum().float()
    fn = ((pr != chosen_class) & (gt == chosen_class) & mask).sum().float()
    tn = ((pr != chosen_class) & (gt != chosen_class) & mask).sum().float()

    return tp, fp, fn, tn


def compute_weights_based_mask(x, weights):
    if not x.device.type == 'cpu':
        mask = torch.ones(x.shape).cuda().bool()
    else:
        mask = torch.ones(x.shape).bool()
    weights = weights.bool()
    for i in range(len(weights)):
        if not weights[i]:
            mask = mask & (x != i)
    return mask


def recall(tp, fp, fn, tn):
    eps = 1e-7
    return (tp + eps) / (tp + fn + eps)


def precision(tp, fp, fn, tn):
    eps = 1e-7
    return (tp + eps) / (tp + fp + eps)


def f1_score(tp, fp, fn, tn):
    beta = 1
    recall_score = recall(tp, fp, fn, tn)
    precision_score = precision(tp, fp, fn, tn)
    score = ((1 + beta ** 2) * (precision_score * recall_score)) / (beta ** 2 * precision_score + recall_score)
    return score


def binary_classification_accuracy(tp, fp, fn, tn):
    """
    expect pr and gt with same size and contains values in {0,...,c}
    :return: accuracy score in FloatTensor
    """
    return (tp + tn) / (tp + fp + fn + tn)


def multi_class_classification_accuracy(pr, gt):
    """
    binary and multi class accuracy
    expect pr and gt with same size and contains values in {0,...,c}
    :return: accuracy score in FloatTensor
    """
    return torch.mean((pr == gt).type(torch.FloatTensor))
