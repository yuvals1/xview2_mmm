import torch
from ..functions import compute_weights_based_mask
from torch.nn import functional as F


def differentiable_f1(pr, gt, beta=1, eps=1e-4):
    tp = torch.sum(gt * pr)
    fp = torch.sum(pr) - tp
    fn = torch.sum(gt) - tp

    score = ((1 + beta ** 2) * tp + eps) / ((1 + beta ** 2) * tp + beta ** 2 * fn + fp + eps)
    return score


def differentiable_f1_mask(pr, gt, mask, beta=1, eps=1e-4):
    tp = torch.sum(gt * pr * mask)
    fp = torch.sum(pr * mask) - tp
    fn = torch.sum(gt * mask) - tp

    score = ((1 + beta ** 2) * tp + eps) / ((1 + beta ** 2) * tp + beta ** 2 * fn + fp + eps)
    return score


def multi_differentiable_f1_mask(pr, gt, weights, beta=1, eps=1e-4):
    mask = compute_weights_based_mask(gt, weights).float()
    f_scores_lst = []
    for i in range(len(weights)):
        gt_i_mask = (gt == i).float()
        pr_i = pr[:, i, :, :].unsqueeze(1)
        tp = torch.sum(gt_i_mask * pr_i * mask)
        fp = torch.sum(pr_i * mask) - tp
        fn = torch.sum(gt_i_mask * mask) - tp

        one_class_f_score = ((1 + beta ** 2) * tp + eps) / ((1 + beta ** 2) * tp + beta ** 2 * fn + fp + eps)
        f_scores_lst.append(0.0001 + one_class_f_score)

    return f_scores_lst


def focal_multi_differentiable_f1_mask(pr, gt, weights, gamma=1, beta=1, eps=1e-4):
    mask = compute_weights_based_mask(gt, weights).float()
    f_scores_lst = []
    for i in range(len(weights)):
        gt_i = (gt == i).float()
        pr_i = pr[:, i, :, :].unsqueeze(1)
        logpt_i = -F.binary_cross_entropy_with_logits(pr_i, gt_i, reduction="none")
        pt_i = torch.exp(logpt_i)
        focal_mask_i = 1 + (1 - pt_i).pow(gamma)

        tp = torch.sum(gt_i * pr_i * mask * focal_mask_i)
        fp = torch.sum(pr_i * mask * focal_mask_i) - tp
        fn = torch.sum(gt_i * mask * focal_mask_i) - tp

        one_class_f_score = ((1 + beta ** 2) * tp + eps) / ((1 + beta ** 2) * tp + beta ** 2 * fn + fp + eps)
        f_scores_lst.append(0.0001 + one_class_f_score)

    return f_scores_lst
