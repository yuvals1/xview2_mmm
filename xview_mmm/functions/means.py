import torch
import numpy as np


def harmonic_mean(scores_lst, weights):
    sum_inverses = 0
    for score, weight in zip(scores_lst, weights):
        sum_inverses += (1 / score) * weight

    return torch.sum(weights) / sum_inverses


def arithmetic_mean(scores_lst, weights):
    sum_scores = 0
    for score, weight in zip(scores_lst, weights):
        sum_scores += score * weight

    return sum_scores / torch.sum(weights)


