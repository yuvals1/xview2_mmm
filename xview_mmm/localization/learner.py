from segmentation_models_pytorch.layers.out_modules import OutSoftmax, OutSigmoid
import matplotlib.pyplot as plt

from ..plotting import tensor_to_img, cmap, bounds, norm
from ..xview_base import XviewSegmentationLearner

prediction_softmax = OutSoftmax(take_argmax_flag=True)
prediction_sigmoid = OutSigmoid(threshold=0.5)
sigmoid = OutSigmoid(threshold=None)


class LocalizationSegmentationLearner(XviewSegmentationLearner):
    task = 'localization'

    def imshow_batch(self, pr, gt, x):

        x_pre_0 = x[0, :, :, :]
        gt_0 = gt[0, :, :, :]
        pr_0_logit = pr[0, :, :, :]
        pr_0 = prediction_sigmoid(pr)[0, :, :, :]

        x_pre_img = tensor_to_img(x_pre_0, mask=False, inverse_preprocess=True)
        gt_img = tensor_to_img(gt_0, mask=True)
        pr_img = tensor_to_img(pr_0, mask=True)
        pr_logit_img = tensor_to_img(pr_0_logit, mask=True)

        fig, ax = plt.subplots(2, 2, figsize=(14, 14))

        fig.tight_layout(pad=5)
        im = ax[0, 0].imshow(x_pre_img); ax[0, 0].set_title('pre')
        im = ax[0, 1].imshow(pr_logit_img); ax[0, 1].set_title('pr before threshold')
        im = ax[1, 0].imshow(gt_img, interpolation='nearest', cmap=cmap, norm=norm); ax[1, 0].set_title('gt')
        im = ax[1, 1].imshow(pr_img, interpolation='nearest', cmap=cmap, norm=norm); ax[1, 1].set_title('pr')

        cbar_ax = fig.add_axes([0.95, 0.32, 0.05, 0.35])
        fig.colorbar(im, cmap=cmap, norm=norm, boundaries=bounds, cax=cbar_ax)
        plt.show()



