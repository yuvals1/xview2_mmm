from ..xview_base import to_numpy, to_tensor
from xview_mmm.xview_base import XviewLoss
from ..import functions
import cv2
import numpy as np
import torch
from torch import nn
from torch.nn import functional as F
from ai_learner.utils import to_snake_case, get_class_name


def make_single_contour_mask(gt):
    h, w = gt.shape
    a = np.zeros((h, w))
    contours, _ = cv2.findContours(gt, cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)

    for c in contours:
        cv2.drawContours(a, c, -1, 1, 3)

    a = cv2.blur(a, (7, 7))
    return a


def make_single_contour_mask_gallil(mask):
    # find connected components
    ret, labels = cv2.connectedComponents(mask)

    # calculate distances
    raw_dist = np.zeros(mask.shape, dtype=np.float32)
    for cc in range(ret - 1):
        # current cc mask only for contour
        cur_cc = (labels == (cc + 1))
        cur_cc = cur_cc.astype(np.uint8)
        pixels_in_cc = np.where(labels == (cc + 1))

        # find contour of CC
        contours, _ = cv2.findContours(cur_cc, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)

        # enumerate over all pixels in connected component and calculate distance
        for i, j in [(pixels_in_cc[0][m], pixels_in_cc[1][m]) for m in range(len(pixels_in_cc[0]))]:
            raw_dist[i, j] = cv2.pointPolygonTest(contours[0], (j, i), True)

    # normalise by maximum distance of each CC
    for j in range(ret - 1):
        raw_dist[labels == (j + 1)] /= np.max(raw_dist[labels == (j + 1)])

    dists = 1 - raw_dist
    dists[labels == 0] = 0

    return dists


def get_contours_mask(gt):

    b = gt.shape[0]
    tensors = []
    for i in range(b):
        numpy_mask = to_numpy(gt[i, :, :, :].detach().cpu())
        contour_mask_numpy = make_single_contour_mask(numpy_mask[:, :, 0].astype('uint8'))
        contour_mask = to_tensor(contour_mask_numpy).cuda()
        tensors.append(contour_mask)

    return torch.stack(tensors, dim=0)


class BCE(XviewLoss):

    bce = nn.BCEWithLogitsLoss()

    def forward(self, pr, gt):
        return self.bce(pr, gt)


class FocalContourBCE(XviewLoss):
    def __init__(self, contour_weight=3, gamma=3, alpha=0.5):
        super().__init__()
        self.contour_weight = contour_weight if contour_weight else 0
        self.gamma = gamma if gamma else 0
        self.alpha = alpha if alpha else 0.5

    def forward(self, pr, gt):
        logpt = -F.binary_cross_entropy_with_logits(pr, gt, reduction="none")
        pt = torch.exp(logpt)
        contour_mask = get_contours_mask(gt) if self.contour_weight else 1
        focal_mask = (1 - pt).pow(self.gamma)
        alpha_mask = self.alpha * gt + (1 - self.alpha) * (1 - gt)

        mask = alpha_mask * focal_mask + self.contour_weight * contour_mask
        loss = mask * (-logpt)
        return loss.mean() * 10


class Dice(XviewLoss):
    def __init__(self, beta=1):
        super().__init__()
        self.sigmoid = torch.nn.Sigmoid()
        self.beta = beta

    def forward(self, pr, gt):
        if 1 not in gt.unique():
            print('skipped')
            return torch.tensor(0., device='cuda', requires_grad=True)

        pr = self.sigmoid(pr)
        score = 1 - functions.differentiable_f1(pr, gt, self.beta)
        return score


class FocalContourDice(XviewLoss):
    def __init__(self, contour_weight=3, gamma=3, alpha=0.5):
        super().__init__()
        self.contour_weight = contour_weight if contour_weight else 0
        self.gamma = gamma if gamma else 0
        self.alpha = alpha if alpha else 0.5
        self.sigmoid = torch.nn.Sigmoid()

    def forward(self, pr, gt):
        logpt = -F.binary_cross_entropy_with_logits(pr, gt, reduction="none")
        pt = torch.exp(logpt)
        contour_mask = get_contours_mask(gt) if self.contour_weight else 1
        focal_mask = (1 - pt).pow(self.gamma)
        alpha_mask = self.alpha * gt + (1 - self.alpha) * (1 - gt)

        mask = alpha_mask * focal_mask + self.contour_weight * contour_mask

        pr = self.sigmoid(pr)
        score = 1 - functions.differentiable_f1_mask(pr, gt, mask)
        return score


class DiceBCE(XviewLoss):
    def __init__(self):
        super().__init__()
        self.__name__ = '_'.join([to_snake_case(get_class_name(self))])

        self.dice = Dice()
        self.bce = BCE()

    def forward(self, pr, gt):
        dice = self.dice(pr, gt)
        bce = self.bce(pr, gt)
        return dice + bce


# for future work
class RectangleLoss(nn.Module):
    __name__ = 'rectangle_loss'

    def __init__(self):
        super().__init__()
        self.__name__ = '_'.join([to_snake_case(get_class_name(self))])

        self.conv = nn.modules.conv.Conv2d(1, 10, (2, 2))
        self.conv.bias = nn.Parameter(torch.tensor([-1., -1., -2., -2., -2., -2., -2., -2., -2., -2.]),
                                      requires_grad=False)
        self.conv.weight[0] = nn.Parameter(torch.tensor([[[1., -1.],
                                                          [-1., 1.]]]), requires_grad=False)
        self.conv.weight[1] = nn.Parameter(torch.tensor([[[-1., 1.],
                                                          [1., -1.]]]), requires_grad=False)
        self.conv.weight[2] = nn.Parameter(torch.tensor([[[-3., 1.],
                                                          [1., 1.]]]), requires_grad=False)
        self.conv.weight[3] = nn.Parameter(torch.tensor([[[1., -3.],
                                                          [1., 1.]]]), requires_grad=False)
        self.conv.weight[4] = nn.Parameter(torch.tensor([[[1., 1.],
                                                          [-3., 1.]]]), requires_grad=False)
        self.conv.weight[5] = nn.Parameter(torch.tensor([[[1., 1.],
                                                          [1., -3]]]), requires_grad=False)
        self.conv.weight[6] = nn.Parameter(-torch.tensor([[[-3., 1.],
                                                           [1., 1.]]]), requires_grad=False)
        self.conv.weight[7] = nn.Parameter(-torch.tensor([[[1., -3.],
                                                           [1., 1.]]]), requires_grad=False)
        self.conv.weight[8] = nn.Parameter(-torch.tensor([[[1., 1.],
                                                           [-3., 1.]]]), requires_grad=False)
        self.conv.weight[9] = nn.Parameter(-torch.tensor([[[1., 1.],
                                                           [1., -3]]]), requires_grad=False)
        self.conv2 = nn.modules.conv.Conv2d(10, 1, (1, 1))
        self.conv2.bias = nn.Parameter(torch.tensor([0.]), requires_grad=False)
        self.conv2.weight[0][0] = nn.Parameter(torch.tensor([[5.]]), requires_grad=False)
        self.conv2.weight[0][1] = nn.Parameter(torch.tensor([[5.]]), requires_grad=False)
        self.conv2.weight[0][2] = nn.Parameter(torch.tensor([[1.]]), requires_grad=False)
        self.conv2.weight[0][3] = nn.Parameter(torch.tensor([[1.]]), requires_grad=False)
        self.conv2.weight[0][4] = nn.Parameter(torch.tensor([[1.]]), requires_grad=False)
        self.conv2.weight[0][5] = nn.Parameter(torch.tensor([[1.]]), requires_grad=False)
        self.conv2.weight[0][6] = nn.Parameter(torch.tensor([[0.]]), requires_grad=False)
        self.conv2.weight[0][7] = nn.Parameter(torch.tensor([[0.]]), requires_grad=False)
        self.conv2.weight[0][8] = nn.Parameter(torch.tensor([[0.]]), requires_grad=False)
        self.conv2.weight[0][9] = nn.Parameter(torch.tensor([[0.]]), requires_grad=False)

        self.relu = nn.ReLU(inplace=True)

        self.conv.to('cuda')
        self.conv2.to('cuda')

    def compute(self, x):
        x = self.conv(x)
        x = self.relu(x)
        x = self.conv2(x)
        x = torch.flatten(x, 1)
        x = torch.sum(x)
        return x

    def forward(self, y_pr, y_gt):
        y_pr = torch.sigmoid(y_pr)
        return self.compute(y_pr) / (self.compute(y_gt) + 0.00001)


# class Focal(XviewLoss):
#     def __init__(self, alpha=0.5, gamma=2, activation='sigmoid'):
#         super().__init__()
#         self.__name__ = '_'.join([to_snake_case(get_class_name(self))])
#
#         assert activation == 'sigmoid', 'not sigmoid'
#         self.focal = toolbelt_focal.BinaryFocalLoss(alpha=alpha, gamma=gamma)
#
#     def forward(self, pr, gt):
#         return self.focal(pr, gt)


