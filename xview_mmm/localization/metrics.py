from xview_mmm.xview_base.metrics.confusion_matrix import F1Score, Recall, Precision


class LocalizationScore(F1Score):
    def __init__(self, activation='sigmoid'):
        super().__init__(activation=activation, labels=[0, 1], chosen_label=1)


class Recall0(Recall):
    def __init__(self, activation='sigmoid'):
        super().__init__(activation=activation, labels=[0, 1], chosen_label=0)


class Precision0(Precision):
    def __init__(self, activation='sigmoid'):
        super().__init__(activation=activation, labels=[0, 1], chosen_label=0)


class Recall1(Recall):
    def __init__(self, activation='sigmoid'):
        super().__init__(activation=activation, labels=[0, 1], chosen_label=1)


class Precision1(Precision):
    def __init__(self, activation='sigmoid'):
        super().__init__(activation=activation, labels=[0, 1], chosen_label=1)




