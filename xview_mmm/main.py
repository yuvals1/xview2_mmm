import pandas as pd
import os
from os.path import join
from sklearn.model_selection import train_test_split
import torch
from torch import nn
import albumentations as albu
from functools import partial
# from torch_lr_finder import LRFinder
import plotly.express as px
import numpy as np
import cv2

import sys

CODE_PATH = '/home/mmm/Desktop/XView_Project/code/segmentation_models.pytorch'
sys.path.append(CODE_PATH)
CODE_PATH = '/home/mmm/Desktop/XView_Project/code/xview2_mmm/'
sys.path.append(CODE_PATH)
CODE_PATH = '/home/mmm/Desktop/XView_Project/code/ai_learner/'
sys.path.append(CODE_PATH)
CODE_PATH = '/home/mmm/Desktop/XView_Project/code/UNet-family/'
sys.path.append(CODE_PATH)

from segmentation_models_pytorch.encoders import get_preprocessing_fn

from ai_learner.ann import EarlyStopping, ReduceLRCB, PlotHistory, unwrap_batch

from xview_mmm.damage import losses, metrics, models, DamageSegmentationLearner

from xview_mmm.xview_base import XviewPhaseMaker, normalize255, to_tensor, ConfusionMatrix, unwrap_inference_batch

from xview_mmm.xview_base.callbacks import ImshowEpoch, SkipUnderAppearanceBatch, ConfusionMatrixCB
from xview_mmm.plotting import get_img_from_folder, imshow_mask, get_img

XVIEW_PROJECT_DIR = '/home/mmm/Desktop/XView_Project'

IMAGES_DIR = join(XVIEW_PROJECT_DIR, 'data', 'images')
MODELS_DIR = join(XVIEW_PROJECT_DIR, 'models')
images_df = pd.read_csv(join(IMAGES_DIR, 'images_df.csv'))

# Dataset partitioning
train_xview_images_df = images_df.query("dataset=='xview_train'")
tier3_xview_images_df = images_df.query("dataset=='xview_tier3'")

train_512_xview_images_df = images_df.query("dataset=='xview_train_512'")
train_256_xview_images_df = images_df.query("dataset=='xview_train_256'")

tier3_512_xview_images_df = images_df.query("dataset=='xview_tier3_512'")
tier3_256_xview_images_df = images_df.query("dataset=='xview_tier3_256'")

train_tier3_256_xview_images_df = images_df.query("dataset=='xview_train_256' or dataset=='xview_tier3_256'")
train_tier3_512_xview_images_df = images_df.query("dataset=='xview_train_512' or dataset=='xview_tier3_512'")

test_xview_images_df = images_df.query("dataset=='xview_test'")

train_tier3_256_xview_images_df_over200 = train_tier3_256_xview_images_df.query("num_building_pixels>200")
train_tier3_512_xview_images_df_over200 = train_tier3_512_xview_images_df.query("num_building_pixels>200")

train, df_valid = train_test_split(train_xview_images_df.query("num_buildings_post>0"),
                                   test_size=0.3, random_state=42)
train = pd.concat([train], axis=0)

train_images = []
for img_name in train_tier3_256_xview_images_df_over200.query("dataset=='xview_train_256'")['img_name']:
    if img_name[:-8] in [item[:-4] for item in list(train['img_name'])]:
        train_images.append(True)
    else:
        train_images.append(False)

df_train = train_tier3_256_xview_images_df_over200.query("dataset=='xview_train_256'")[train_images]

encoder_name = 'efficientnet-b0'
encoder_weights = 'imagenet'

model = models.TwoEncodersUnet(encoder_name=encoder_name, encoder_weights=encoder_weights)
from unet_family.networks.UNet_Nested import UNet_Nested
# from xview_mmm.e2e.att_models import UnetPP

# model = UnetPP(encoder_name=encoder_name, encoder_weights=encoder_weights)
# model = UNet_Nested(in_channels=3, n_classes=6)

# sum(p.numel() for p in model.parameters())

MODEL_NAME = '_'.join(['unet_pp_on_damage', encoder_name, 'e2e', '256', 'started_from_imagenet'])
# MODEL_NAME = '_'.join(['unet', encoder_name, 'e2e','random_512','from_imagenet'])

MODELS_TASK_DIR = join(MODELS_DIR, 'e2e_seg')
print('model name:', MODEL_NAME)

loss = losses.Dice(torch.tensor([0, 1., 1., 1., 1., 0.], device='cuda'))

optimizer = torch.optim.Adam(params=[{'params': model.parameters(), 'lr': 1e-3}], lr=1e-2,
                             eps=1e-04)

learner_metrics = [metrics.DamageScore(),
                   metrics.F1_1(), metrics.F1_2(), metrics.F1_3(), metrics.F1_4(),
                   metrics.Recall1(), metrics.Recall2(), metrics.Recall3(), metrics.Recall4(),
                   metrics.Precision1(), metrics.Precision2(), metrics.Precision3(), metrics.Precision4(),
                   ]

learner = DamageSegmentationLearner(model, loss, optimizer, learner_metrics, apex=False, device='cuda')

learner.config_for_saving(MODEL_NAME, MODELS_TASK_DIR)

learner.model = nn.DataParallel(learner.model)

# learner phases configuration for training
constant_augmentations = [
    #                         albu.RandomCrop(512, 512),
    #                         albu.Resize(512, 512),
    albu.RandomRotate90(p=1),
    albu.Transpose(p=0.5),
    #                         albu.ShiftScaleRotate(scale_limit=(0.1, 0.1), rotate_limit=45, shift_limit=0, p=0.4),
    #                         albu.MaskDropout((0,5), p=0.5),
    #                         albu.ToGray(p=0.3),

]

wild_augmentations = [
    #     albu.RandomFog(fog_coef_upper=0.5),
    #     albu.RandomRain(p=1),
    albu.RandomGamma(gamma_limit=(20, 200), p=1),

    albu.ToGray(p=1),
    #     albu.Blur(blur_limit=9, p=1),
    #     albu.CoarseDropout(max_holes=20, max_height=14, max_width=14, p=1),
    albu.RGBShift(p=1),
    albu.RandomBrightnessContrast(p=1),
    albu.CLAHE(p=1),
]

augmentation = [
    albu.Compose(constant_augmentations),
    #         albu.OneOf(wild_augmentations, p=0.7),
    #         albu.OneOf(wild_augmentations, p=0.5),
]

preprocessing = [
    albu.Lambda(image=partial(normalize255)),
    #     albu.Lambda(image=partial(get_preprocessing_fn(encoder_name, 'imagenet'))),
    albu.Lambda(image=partial(to_tensor), mask=partial(to_tensor)),
]

input_folders = ['pre', 'post']
target_folders = ['post_mask']
phase_maker = XviewPhaseMaker(learner, IMAGES_DIR)

training_phase = phase_maker.set_training_phase(df_train, input_folders, target_folders, preprocessing=preprocessing,
                                                augmentation=augmentation, bs=1)
validation_phase = phase_maker.set_validation_phase(df_valid, input_folders, target_folders,
                                                    # augmentation=[albu.RandomCrop(512, 512)],
                                                    preprocessing=preprocessing, bs=20, shuffle=True)

callbacks = [EarlyStopping(190), ReduceLRCB(optimizer, patience=4, factor=0.5),
             ImshowEpoch('val_and_train'),
             PlotHistory(),
             SkipUnderAppearanceBatch(range(1, 5)),
             #              ConfusionMatrixCB('softmax2d', [0,1,2,3,4,5])
             ]

# 512 damage bs 32 only post
learner.train(training_phase, validation_phase, callbacks=callbacks, epochs=200)