import shutil
import os
import json
import pandas as pd
import cv2
import numpy as np
from matplotlib.path import Path
import shapely.wkt
from unet_family.utils import create_dir_and_clear_if_exists
from os.path import join

xview_project_folder = '/home/mmm/Desktop/XView_Project'
all_datasets_folder = os.path.join(xview_project_folder, 'all_datasets')
xview_datasets = os.path.join(all_datasets_folder, 'xview')
data_folder = os.path.join(xview_project_folder, 'data')
images_folder = os.path.join(data_folder, 'images')
buildings_folder = os.path.join(data_folder, 'buildings')

damage_name2num = {'destroyed': 4, 'major-damage_segmentation': 3, 'minor-damage_segmentation': 2, 'no-damage_segmentation': 1,
                   'un-classified': 5, 'pre': 1}


def move_image(image_name, new_image_name, src_folder, dst_folder):
    old_path = os.path.join(src_folder, image_name)
    new_path = os.path.join(dst_folder, new_image_name)
    # shutil.move(old_path, new_path)
    shutil.copyfile(old_path, new_path)


def copy_pre_post_images_to_different_folders(src_folder, pre_folder, post_folder,
                                              dataset='xview'):
    dst_folders = [pre_folder, post_folder]
    if dataset == 'xview_train' or dataset == 'xview_tier3':
        timings = ['_pre_disaster', '_post_disaster']
    elif dataset == 'xview_test':
        timings = ['_pre', '_post']
    else:
        raise ValueError('no such src set')

    samples = []
    for image_name in sorted(os.listdir(src_folder)):
        if timings[0] in image_name:
            new_image_name = image_name.replace(timings[0], '')
            move_image(image_name, new_image_name, src_folder, dst_folders[0])

            sample = dict()
            sample['img_name'] = new_image_name
            sample['dataset'] = dataset
            samples.append(sample)

        if timings[1] in image_name:
            new_image_name = image_name.replace(timings[1], '')
            move_image(image_name, new_image_name, src_folder, dst_folders[1])

    df = pd.DataFrame(samples)
    return df


def copy_all_xview_sets(train_src_folder, tier3_src_folder, test_src_folder,
                        images_dst_root, create_masks=False):
    dfs = list()
    dfs.append(copy_pre_post_images_to_different_folders(train_src_folder + '/images',
                                                         join(images_dst_root, 'xview_train', 'pre'),
                                                         join(images_dst_root, 'xview_train', 'post'),
                                                         dataset='xview_train'))
    dfs.append(copy_pre_post_images_to_different_folders(tier3_src_folder + '/images',
                                                         join(images_dst_root, 'xview_tier3', 'pre'),
                                                         join(images_dst_root, 'xview_tier3', 'post'),
                                                         dataset='xview_tier3'))
    dfs.append(copy_pre_post_images_to_different_folders(test_src_folder + '/images',
                                                         join(images_dst_root, 'xview_test', 'pre'),
                                                         join(images_dst_root, 'xview_test', 'post'),
                                                         dataset='xview_test'))

    metadata_df = create_xview_metadata_df_and_masks(train_src_folder, tier3_src_folder,
                                                     create_masks=create_masks)

    copy_pre_post_images_to_different_folders(train_src_folder + '/gt_masks',
                                              join(images_dst_root, 'xview_train', 'pre_mask'),
                                              join(images_dst_root, 'xview_train', 'post_mask'),
                                              dataset='xview_train')
    copy_pre_post_images_to_different_folders(tier3_src_folder + '/gt_masks',
                                              join(images_dst_root, 'xview_tier3', 'pre_mask'),
                                              join(images_dst_root, 'xview_tier3', 'post_mask'),
                                              dataset='xview_tier3')
    xview_df = pd.concat(dfs, axis=0)

    xview_df = pd.merge(xview_df, metadata_df, how='left', on='img_name')

    return xview_df


def create_folders_and_df(base_folder=images_folder,
                          train_src_folder=xview_datasets + '/train',
                          tier3_src_folder=xview_datasets + '/tier3',
                          test_src_folder=xview_datasets + '/test',
                          create_masks=False):
    xview_df = copy_all_xview_sets(train_src_folder, tier3_src_folder, test_src_folder,
                                   base_folder, create_masks=create_masks)

    images_df_full = pd.concat([xview_df], axis=0)
    old_col_names = list(images_df_full.columns)
    new_col_names = [x.replace('_x', '_pre').replace('_y', '_post') for x in old_col_names]
    images_df_full = images_df_full.rename(columns=dict(zip(old_col_names, new_col_names)))

    images_df = images_df_full[['img_name', 'dataset', 'disaster', 'disaster_type',
                                'num_buildings_pre', 'num_buildings_post']]

    images_df_full.to_csv(os.path.join(base_folder, 'images_df_full.csv'), index=False)
    images_df.to_csv(os.path.join(base_folder, 'images_df.csv'), index=False)

    return


# json
def extract_json_data_from_labels_folder(labels_folder):
    json_data = []
    for image_name in sorted(os.listdir(labels_folder)):
        with open(os.path.join(labels_folder, image_name), "r") as metadata_file:
            json_data.append(json.load(metadata_file))
    return json_data


def json_data_to_images_df(json_data):
    images_data = []
    for i in range(len(json_data)):
        image_data = json_data[i]['metadata']
        image_data['buildings_data'] = json_data[i]['features']
        image_data['num_buildings'] = len(json_data[i]['features']['lng_lat'])
        images_data.append(image_data)
    images_df = pd.DataFrame(images_data)
    return images_df


def create_mask_for_im(img_name, buildings_data, base_folder):
    h, w = (1024, 1024)
    y, x = np.mgrid[:h, :w]

    buildings_coors = buildings_data['xy']
    if len(buildings_coors) == 0:
        img = np.zeros((h, w))
    else:
        img_polygons = []
        damage_types = []
        for building_coor in buildings_coors:
            img_polygons.append(shapely.wkt.loads(building_coor['wkt']))
            if 'subtype' in building_coor['properties'].keys():
                damage_types.append(damage_name2num[building_coor['properties']['subtype']])
            else:
                damage_types.append(damage_name2num['pre'])

        masks = []
        for poly, damage_type in zip(img_polygons, damage_types):
            polygons_pts = [p for p in poly.exterior.coords]
            points = np.transpose((x.ravel(), y.ravel()))
            path = Path(polygons_pts)
            mask = path.contains_points(points)
            reshaped_mask = mask.reshape(h, w)
            masks.append(reshaped_mask * damage_type)

        img = np.max(masks, axis=0)

    cv2.imwrite(os.path.join(base_folder, img_name), img)
    print(img_name)


def create_xview_metadata_df_and_masks(train_src_folder=xview_datasets + '/train',
                                       tier3_src_folder=xview_datasets + '/tier3', create_masks=False):
    train_json_data = extract_json_data_from_labels_folder(train_src_folder + '/labels')
    train_images_df = json_data_to_images_df(train_json_data)
    if create_masks:
        train_gt_masks_folder = create_dir_and_clear_if_exists(train_src_folder, 'gt_masks')
        train_images_df.apply(
            lambda row: create_mask_for_im(row['img_name'], row['buildings_data'], train_gt_masks_folder),
            axis=1)

    tier3_json_data = extract_json_data_from_labels_folder(tier3_src_folder + '/labels')
    tier3_images_df = json_data_to_images_df(tier3_json_data)
    if create_masks:
        tier3_gt_masks_folder = create_dir_and_clear_if_exists(tier3_src_folder, 'gt_masks')
        tier3_images_df.apply(
            lambda row: create_mask_for_im(row['img_name'], row['buildings_data'], tier3_gt_masks_folder),
            axis=1)

    all_train_df = pd.concat([train_images_df, tier3_images_df], axis=0)
    all_train_df.index = range(len(all_train_df))

    all_train_df_pre = all_train_df[all_train_df['img_name'].str.contains('pre')].copy()
    all_train_df_post = all_train_df[all_train_df['img_name'].str.contains('post')].copy()

    all_train_df_pre['img_name'] = all_train_df_pre['img_name'].apply(lambda x: x.replace('_pre_disaster', ''))
    all_train_df_post['img_name'] = all_train_df_post['img_name'].apply(lambda x: x.replace('_post_disaster', ''))
    all_train_df_post = all_train_df_post.drop(['disaster', 'disaster_type'], axis=1)
    all_train_pairs = pd.merge(all_train_df_pre, all_train_df_post, how='inner', on='img_name')

    return all_train_pairs
