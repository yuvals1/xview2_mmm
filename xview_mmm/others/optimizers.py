from torch import optim


class Adam(optim.Adam):
    __name__ = 'smp_adam_optimizer'

    def __init__(self, model, encoder_lr=1e-3, decoder_lr=1e-2):
        params = [
                {'params': model.encoder.parameters(), 'lr': encoder_lr},
                {'params': model.decoder.parameters(), 'lr': decoder_lr},
                 ]
        super().__init__(params=params, eps=1e-4)
