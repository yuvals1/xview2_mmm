import os
import cv2
import matplotlib.pyplot as plt
import matplotlib
import torch
from ..xview_base.utils import to_numpy
import numpy as np

cmap = matplotlib.colors.ListedColormap(['black', 'white', 'pink', 'orange', 'red', 'green'])
bounds = [-0.5, 0.5, 1.5, 2.5, 3.5, 4.5, 5.5]
norm = matplotlib.colors.BoundaryNorm(bounds, cmap.N)


def get_img(img_name, folder_name, dataset, root_to_folder='/home/mmm/Desktop/XView_Project/data/images'):
    """
    Return numpy image based on image name and folder.
    """
    img_path = os.path.join(root_to_folder, dataset, folder_name, img_name)
    assert os.path.exists(img_path) or os.path.exists(img_path+'.npy'), 'img path not exist: '+img_path

    if 'inst' in folder_name:
        img = np.load(img_path+'.npy')
    elif 'mask' in folder_name:
        img = cv2.imread(img_path, 0)
    else:
        img = cv2.imread(img_path)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

    return img


def get_img_from_folder(img_name, folder_name, root_path):
    """
    Return numpy image based on image name and folder.
    """
    img_path = os.path.join(root_path, folder_name, img_name)
    assert os.path.exists(img_path), 'img path not exist: '+img_path

    if 'mask' in folder_name:
        img = cv2.imread(img_path, 0)
    else:
        img = cv2.imread(img_path)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

    return img


def imshow(img, s=6):
    if type(img) == torch.Tensor:
        img = to_numpy(img.detach().cpu())

    fig, ax = plt.subplots(1, 1, figsize=(s, s))
    im = ax.imshow(img)


def imshow_mask(img, s=6):
    if type(img) == torch.Tensor:
        img = to_numpy(img.detach().cpu())[:, :, 0]

    fig, ax = plt.subplots(1, 1, figsize=(s, s))
    im = ax.imshow(img, interpolation='nearest', cmap=cmap, norm=norm)
    cbar_ax = fig.add_axes([0.95, 0.32, 0.05, 0.35])
    fig.colorbar(im, cmap=cmap, norm=norm, boundaries=bounds, cax=cbar_ax)


def tensor_to_img(tensor, mask=False, inverse_preprocess=False):
    if mask:
        img = to_numpy(tensor.detach().cpu())[:, :, 0]
    else:
        img = to_numpy(tensor.detach().cpu())
        if inverse_preprocess:
            img -= img.min()
            if (img.max() - img.min()) == 0:
                print('oh no')
            img /= (img.max() - img.min())
    return img


