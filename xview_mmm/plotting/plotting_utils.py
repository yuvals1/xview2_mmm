from segmentation_models_pytorch.layers.out_modules import OutSoftmax, OutSigmoid
from . import imshow, imshow_mask
import matplotlib.pyplot as plt

prediction_softmax = OutSoftmax(take_argmax_flag=True)
prediction_sigmoid = OutSigmoid(threshold=0.5)
sigmoid = OutSigmoid(threshold=None)


def plot_gt(gt):
    gt_0 = gt[0, :, :, :]
    imshow_mask(gt_0)
    plt.title('gt')
    plt.show()


def plot_pr_where_gt(pr, gt):
    pr_0 = prediction_softmax(pr)[0, :, :, :]
    gt_0 = gt[0, :, :, :]

    imshow_mask(pr_0 * (gt_0 > 0).float())
    plt.title('pr where gt>0')
    plt.show()


def plot_pr(pr, activation='sigmoid'):
    if activation == 'sigmoid':
        pr_0 = prediction_sigmoid(pr)[0, :, :, :]
    elif activation == 'softmax2d':
        pr_0 = prediction_softmax(pr)[0, :, :, :]
    else:
        raise ValueError('...')
    imshow_mask(pr_0)
    plt.title('pr')
    plt.show()
