import numpy as np

from ai_learner.ann import Callback

from ..xview_base.metrics.confusion_matrix import ConfusionMatrix
from ..e2e_2.metrics import MultiTargetConfusionMatrix


class ImshowEpoch(Callback):
    def __init__(self, how='val'):
        self.flag = True
        self.how = how

    def epoch_started(self, **kwargs):
        self.flag = True

    def phase_started(self, **kwargs):
        self.flag = True

    def batch_ended(self, learner, phase, pr, gt, x, **kwargs):
        if self.how == 'val':
            condition = self.flag and phase.name == 'validation'
        elif self.how == 'val_and_train':
            condition = self.flag
        else:
            raise ValueError('...')

        if condition:
            learner.imshow_batch(pr, gt, x)
            self.flag = False


class ConfusionMatrixCB(Callback):
    def __init__(self, activation, labels):
        self.CM = ConfusionMatrix(activation, labels)
        self.cm_train = 0
        self.cm_valid = 0

    def batch_ended(self, learner, phase, pr, gt, x, **kwargs):
        if phase.name == 'training':
            self.cm_train += self.CM(pr, gt)
        elif phase.name == 'validation':
            self.cm_valid += self.CM(pr, gt)

    def epoch_ended(self, learner, **kwargs):
        cm_train = np.round(self.cm_train / np.sum(self.cm_train), 3)
        cm_valid = np.round(self.cm_valid / np.sum(self.cm_valid), 3)
        print('training: ')
        print(cm_train)
        print('validation: ')
        print(cm_valid)


def check_if_relevant_classes_included(gt, relevant_classes):
    relevant_classes_included = True
    for i in relevant_classes:
        if i not in gt.unique():
            relevant_classes_included = False
    return relevant_classes_included


class SkipUnderAppearanceBatch(Callback):

    def __init__(self, relevant_classes):
        self.relevant_classes = relevant_classes

    def before_forward_pass(self, learner, gt, phase, **kwargs):
        if phase.name == 'training':
            relevant_classes_included = check_if_relevant_classes_included(gt, self.relevant_classes)
            if relevant_classes_included is False:
                print(gt.unique())
                learner.continue_to_next_batch = True


class SeparateBuildingsNature(Callback):

    def before_forward_pass(self, learner, x, gt, **kwargs):
        pre = x[0]
        post = x[1]

        pre_b = pre * gt
        post_b = post * gt
        pre_nb = pre * (1 - gt)
        post_b = post * (1 - gt)


class SkipUnderAppearanceBatchE2E2(Callback):

    def __init__(self, relevant_classes):
        self.relevant_classes = relevant_classes

    def before_forward_pass(self, learner, gt, phase, **kwargs):
        if phase.name == 'training':
            print('skrt')
            relevant_classes_included = check_if_relevant_classes_included(gt, self.relevant_classes)
            if relevant_classes_included is False:
                print(gt.unique())
                learner.continue_to_next_batch = True


# class ConfusionMatrixCBE2E2(Callback):
#     def __init__(self, activation, labels):
#         self.CM = MultiTargetConfusionMatrix()
#         self.cm_train = 0
#         self.cm_valid = 0
#
#     def batch_ended(self, learner, phase, pr, gt, x, **kwargs):
#         if phase.name == 'training':
#             self.cm_train += self.CM(pr, gt)
#         elif phase.name == 'validation':
#             self.cm_valid += self.CM(pr, gt)
#
#     def epoch_ended(self, learner, **kwargs):
#         cm_train = np.round(self.cm_train / np.sum(self.cm_train), 3)
#         cm_valid = np.round(self.cm_valid / np.sum(self.cm_valid), 3)
#         print('training: ')
#         print(cm_train)
#         print('validation: ')
#         print(cm_valid)
