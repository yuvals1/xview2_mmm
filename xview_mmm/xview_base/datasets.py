import os
from os.path import join
from torch.utils.data import Dataset
import pandas as pd
import albumentations as albu

from ..plotting import get_img_from_folder, get_img

disasters = ['guatemala-volcano', 'hurricane-florence', 'hurricane-harvey',
             'hurricane-matthew', 'hurricane-michael', 'mexico-earthquake',
             'midwest-flooding', 'palu-tsunami', 'santa-rosa-wildfire',
             'socal-fire']

disaster_2_num = {k: i for i, k in zip(range(len(disasters)), disasters)}

cols_2_dict = {'disaster': disaster_2_num}


def col_2_num(meta, col):
    d = cols_2_dict[col]
    return d[meta[col]]


def apply_transformations(tfms, sample, main_image):
    sample['image'] = sample.pop(main_image)
    additional_targets = {}
    for im_type in sample.keys():
        if im_type == 'image':
            continue
        mask = True if 'mask' in im_type else False
        additional_targets[im_type] = 'mask' if mask else 'image'

    sample = albu.Compose(tfms, p=1, additional_targets=additional_targets)(**sample)
    sample[main_image] = sample.pop('image')

    return sample


class DfDataset(Dataset):
    def __init__(self, csv_file):
        super().__init__()
        if type(csv_file) == str:
            self.df = pd.read_csv(csv_file)
        else:
            self.df = csv_file

    def __len__(self):
        return len(self.df)


class ImagesDfDataset(DfDataset):
    def __init__(self,
                 csv_file,
                 data_dir,
                 input_img_folders,
                 target_img_folders=None,
                 target_cols=None,
                 augmentation=None,
                 preprocessing=None,
                 ):

        super().__init__(csv_file=csv_file)
        self.data_dir = data_dir

        self.only_input = target_img_folders is None

        self.input_img_types = input_img_folders
        self.target_img_types = [] if self.only_input else target_img_folders
        self.target_cols = target_cols if target_cols else []

        self.augmentation = augmentation
        self.preprocessing = preprocessing

    def __getitem__(self, idx):

        meta = self.df.iloc[idx]
        sample = dict()

        for img_type in self.input_img_types + self.target_img_types:
            img_name = meta['img_name']
            dataset = meta['dataset']
            sample[img_type] = get_img(img_name, folder_name=img_type, dataset=dataset, root_to_folder=self.data_dir)

        main_image = self.input_img_types[0]
        if self.augmentation:
            sample = apply_transformations(self.augmentation, sample, main_image=main_image)
        if self.preprocessing:
            sample = apply_transformations(self.preprocessing, sample, main_image=main_image)

        x_tup = tuple([sample[k] for k in self.input_img_types])

        if self.only_input:
            return x_tup, meta['img_name'], meta['dataset']
        else:
            y_tup = tuple([sample[k] for k in self.target_img_types] + [col_2_num(meta, col) for col in self.target_cols])
            return x_tup, y_tup

#
# class OneImageAugDataset(DfDataset):
#     def __init__(self,
#                  csv_file,
#                  data_dir,
#                  input_img_folders,
#                  target_img_folders=None,
#                  augmentation=None,
#                  preprocessing=None,
#                  ):
#         super().__init__(csv_file=csv_file)
#         self.data_dir = data_dir
#
#         self.input_img_types = input_img_folders
#         self.target_img_types = [] if self.only_input else target_img_folders
#
#         self.augmentation = augmentation
#         self.preprocessing = preprocessing
#
#     def __getitem__(self, idx):
#
#         meta = self.df.iloc[idx]
#         sample = dict()
#
#         for img_type in self.input_img_types + self.target_img_types:
#             img_name = meta['img_name']
#             dataset = meta['dataset']
#             sample[img_type] = get_img(img_name, folder_name=img_type, dataset=dataset, root_to_folder=self.data_dir)
#
#         main_image = self.input_img_types[0]
#         if self.preprocessing:
#             sample = apply_transformations(self.preprocessing, sample, main_image=main_image)
#

class OneImageAugDataset(DfDataset):
    def __init__(self,
                 csv_file,
                 data_dir,
                 input_img_folders,
                 target_img_folders=None,
                 target_cols=None,
                 augmentation=None,
                 preprocessing=None,
                 ):

        super().__init__(csv_file=csv_file)
        self.data_dir = data_dir

        self.input_img_types = input_img_folders
        self.target_img_types = [] if self.only_input else target_img_folders
        self.target_cols = target_cols if target_cols else []

        self.augmentation = augmentation
        self.preprocessing = preprocessing

    def __getitem__(self, idx):

        meta = self.df.iloc[idx]
        sample = dict()

        for img_type in self.input_img_types + self.target_img_types:
            img_name = meta['img_name']
            dataset = meta['dataset']
            sample[img_type] = get_img(img_name, folder_name=img_type, dataset=dataset, root_to_folder=self.data_dir)

        main_image = self.input_img_types[0]
        if self.augmentation:
            sample = apply_transformations(self.augmentation, sample, main_image=main_image)
        if self.preprocessing:
            sample = apply_transformations(self.preprocessing, sample, main_image=main_image)

        x_tup = tuple([sample[k] for k in self.input_img_types])

        if self.only_input:
            return x_tup, meta['img_name'], meta['dataset']
        else:
            y_tup = tuple([sample[k] for k in self.target_img_types] + [col_2_num(meta, col) for col in self.target_cols])
            return x_tup, y_tup


class MatchingFoldersDataset(Dataset):
    def __init__(self,
                 *folders_names,
                 root_dir_path,
                 img_names=None
                 ):
        self.folders_names = folders_names
        self.root_dir_path = root_dir_path
        if img_names is None:
            self.img_names = sorted(os.listdir(join(self.root_dir_path, self.folders_names[0])))
        else:
            self.img_names = img_names

    def __len__(self):
        len(self.img_names)

    def __getitem__(self, idx):
        img_name = self.img_names[idx]
        images = []
        for folder in self.folders_names:
            images.append(get_img_from_folder(img_name, folder, self.root_dir_path))

        return images, img_name


# out of use

class GeneralDataset(DfDataset):
    def __init__(self,
                 csv_file,
                 data_dir,
                 input_img_folders,
                 target_img_folders,
                 inputs_from_df,
                 targets_from_df,
                 augmentation=None,
                 preprocessing=None,
                 ):

        super().__init__(csv_file)

        self.data_dir = data_dir
        self.input_img_folders = input_img_folders
        self.target_img_folders = target_img_folders
        self.img_folders = self.input_img_folders + self.target_img_folders
        self.inputs_from_df = inputs_from_df
        self.targets_from_df = targets_from_df
        self.from_df = self.inputs_from_df + self.targets_from_df
        self.augmentation = augmentation
        self.preprocessing = preprocessing

    def __getitem__(self, idx):

        meta = self.df.iloc[idx]
        sample = dict()

        if self.img_folders:
            for img_folder in self.img_folders:
                img_name = meta['img_name']
                sample[img_folder[:-1]] = get_img(img_name, folder_name=img_folder, root_to_folder=self.data_dir)

            main_image = self.img_folders[0]
            if self.augmentation:
                sample = apply_transformations(self.augmentation, sample, main_image=main_image)
            if self.preprocessing:
                sample = apply_transformations(self.preprocessing, sample, main_image=main_image)

        if self.from_df:
            for col in self.from_df:
                sample[col] = meta[col]

        x_list = [sample[k] for k in self.input_img_folders + self.inputs_from_df]
        y_list = [sample[k] for k in self.target_img_folders + self.targets_from_df]

        return x_list, y_list

