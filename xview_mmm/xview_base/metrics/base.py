import numpy as np
from ai_learner.utils import to_snake_case, get_class_name
from torch import nn


class Metric(nn.Module):
    def __init__(self):
        super().__init__()
        self.__name__ = to_snake_case(get_class_name(self))


class XviewLoss(Metric):

    def batch_update(self, pr, gt):
        return self(pr, gt).detach().item()

    def epoch_update(self, batches_scores_list):
        return np.mean(batches_scores_list)


class XviewMetric(Metric):

    def batch_update(self, pr, gt):
        raise NotImplementedError

    def epoch_update(self, batches_scores_list):
        raise NotImplementedError

    def forward(self, pr, gt):
        return self.epoch_update([self.batch_update(pr, gt)])


