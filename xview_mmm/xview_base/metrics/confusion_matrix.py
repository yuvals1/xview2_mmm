import numpy as np

from segmentation_models_pytorch.layers.out_modules import PredictionLayer

from . import XviewMetric
from . import confusion_matrix_compute, recall, precision, f1_score, numpy_harmonic_mean


class ConfusionMatrix(XviewMetric):
    def __init__(self, activation, labels, chosen_label=None, pre_post=False):
        super().__init__()
        self.prediction_layer = PredictionLayer(activation)
        self.activation = activation
        self.labels = labels
        self.chosen_label = chosen_label
        self.pre_post = pre_post

    def batch_update(self, pr, gt):
        if self.pre_post:
            gt = gt[1]
        pr = self.prediction_layer(pr)
        cm = confusion_matrix_compute(pr, gt, self.labels)
        return cm

    def epoch_update(self, batches_scores_list):
        cm = np.sum(batches_scores_list, axis=0)
        return cm


class ConfusionMatrixReg(XviewMetric):
    def __init__(self):
        super().__init__()
        self.labels = [0, 1, 2, 3, 4, 5]

    def batch_update(self, pr, gt):

        pr = pr.round()
        cm = confusion_matrix_compute(pr, gt, self.labels)
        return cm

    def epoch_update(self, batches_scores_list):
        cm = np.sum(batches_scores_list, axis=0)
        return cm


class F1Score(ConfusionMatrix):
    def epoch_update(self, batches_scores_list):
        cm = np.sum(batches_scores_list, axis=0)
        index = self.labels.index(self.chosen_label)
        return f1_score(cm, index)


class Recall(ConfusionMatrix):
    def epoch_update(self, batches_scores_list):
        cm = np.sum(batches_scores_list, axis=0)
        index = self.labels.index(self.chosen_label)
        return recall(cm, index)


class Precision(ConfusionMatrix):
    def epoch_update(self, batches_scores_list):
        cm = np.sum(batches_scores_list, axis=0)
        index = self.labels.index(self.chosen_label)
        return precision(cm, index)


class HarmonicF1Score(ConfusionMatrix):
    def epoch_update(self, batches_scores_list):
        cm = np.sum(batches_scores_list, axis=0)
        scores = []
        for label in self.labels:
            index = self.labels.index(label)
            scores.append(f1_score(cm, index))

        return numpy_harmonic_mean(scores)


# class E2EConfusionMatrix(XviewMetric):
#     def __init__(self, activation):
#         super().__init__()
#         self.prediction_layer = PredictionLayer(activation)
#
#     def batch_update(self, pr, gt):
#         pr = self.prediction_layer(pr)
#         cm = confusion_matrix_compute(pr, gt, self.labels)
#         return cm
#
#     def epoch_update(self, batches_scores_list):
#         cm = np.sum(batches_scores_list, axis=0)
#         return cm


class TwoOutputsScore(XviewMetric):
    def __init__(self, class_a, class_b, weight_a, weight_b):
        super().__init__()
        self.a = class_a()
        self.b = class_b()
        self.weight_a = weight_a
        self.weight_b = weight_b

    def batch_update(self, pr, gt):
        a_batch_score = self.a.batch_update(pr[0], gt[0])
        b_batch_score = self.b.batch_update(pr[1], gt[1])

        return {'a': a_batch_score, 'b': b_batch_score}

    def epoch_update(self, batches_scores_list):

        a = []
        b = []
        for batch_score in batches_scores_list:
            a.append(batch_score['a'])
            b.append(batch_score['b'])

        total_score = self.weight_a * self.a.epoch_update(a) + self.weight_b * self.b.epoch_update(b)
        return total_score


