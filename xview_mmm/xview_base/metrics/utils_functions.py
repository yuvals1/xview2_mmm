import numpy as np


def confusion_matrix_compute(pr, gt, labels):
    num_labels = len(labels)
    cm = np.zeros((num_labels, num_labels))
    for i, label_i in enumerate(labels):
        for j, label_j in enumerate(labels):
            if type(pr) == np.ndarray:
                cm[i, j] = ((pr == label_i) & (gt == label_j)).sum()
            else:
                cm[i, j] = ((pr == label_i) & (gt == label_j)).sum().float().cpu().numpy()
    return cm


def recall(cm, index):
    tp = cm[index, index]
    actual = np.sum(cm[:, index])
    if actual > 0:
        return tp / actual
    else:
        return 1


def precision(cm, index):
    tp = cm[index, index]
    predicted = np.sum(cm[index, :])
    if predicted > 0:
        return tp / predicted
    else:
        return 1


def f1_score(cm, index):
    beta = 1
    recall_score = recall(cm, index)
    precision_score = precision(cm, index)
    if precision_score + recall_score > 0:
        return ((1 + beta ** 2) * (precision_score * recall_score)) / (beta ** 2 * precision_score + recall_score)
    else:
        return 1e-4


def numpy_harmonic_mean(scores_lst, weights=None):
    if weights is None:
        weights = np.ones(len(scores_lst))
    sum_inverses = 0
    for score, weight in zip(scores_lst, weights):
        sum_inverses += (1 / score) * weight

    return np.sum(weights) / sum_inverses
