import torch

from ai_learner.ann import to_device


def to_numpy(x, **kwargs):
    """
    Convert image or mask.
    """
    if len(x.shape) == 3:
        x = x.numpy().transpose(1, 2, 0)
    else:
        raise ValueError('not yet')

    return x


def to_tensor(x, **kwargs):
    """
    Convert image or mask.
    """
    # image
    if len(x.shape) == 3:
        x = torch.from_numpy(x.transpose(2, 0, 1)).type(torch.FloatTensor)

    # mask
    else:
        x = torch.from_numpy(x).unsqueeze(0).type(torch.FloatTensor)
    return x


def normalize255(x, **kwargs):

    x = x / 255
    return x


def unwrap_inference_batch(batch, device):
    x, name, dataset = batch
    x = to_device(x, device)
    return x, name, dataset


