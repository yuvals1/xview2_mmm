import os
from os.path import join
import numpy as np
import cv2

from ai_learner.ann import AnnLearner

from . import to_numpy, unwrap_inference_batch


class XviewSegmentationLearner(AnnLearner):
    task = NotImplemented

    def infer(self, inference_phase, prediction_type='mask', device='cuda', dest_dir=None):

        # self.load_best_model()
        # self.model.to(device)
        self.model.train(False)

        self.inference_phase = inference_phase

        dest_dir = inference_phase.data_dir if dest_dir is None else dest_dir

        for batch in inference_phase.loader:
            self.infer_single_batch(batch, device, prediction_type, dest_dir=dest_dir)

    def infer_single_batch(self, batch, device, prediction_type, dest_dir):

        x, img_name, dataset = unwrap_inference_batch(batch, device)
        img_name = img_name[0]
        dataset = dataset[0]

        prediction_dir_name = '_'.join([self.model_name, prediction_type, str(self.best_model_score)[:6]])
        img_path = make_img_path_for_inference(dest_dir, dataset, prediction_dir_name,
                                               img_name, self.task)
        if prediction_type is 'prob':
            x = self.model.predict_prob(x)

        elif prediction_type is 'mask':
            x = self.model.predict(x)
        else:
            raise ValueError('...')

        save_mask(x=x, path=img_path, prediction_type=prediction_type)


def make_img_path_for_inference(data_dir, dataset, prediction_dir_name, img_name, task):

    prediction_dir_path = join(data_dir, dataset, prediction_dir_name)

    if not os.path.exists(prediction_dir_path):
        os.mkdir(prediction_dir_path)

    if 'test' in img_name:
        num = img_name.split('_')[1].split('.')[0]
        img_name = '_'.join(['test', task, str(num), 'prediction.png'])

    print(img_name)
    img_path = join(prediction_dir_path, img_name)
    return img_path


def save_mask(x, path, prediction_type='mask'):
    if prediction_type is 'mask':
        mask = to_numpy(x[0, :, :, :].detach().cpu())[:, :, 0]
        cv2.imwrite(path, mask)
    elif prediction_type is 'prob':
        mask = to_numpy(x[0, :, :, :].detach().cpu())
        mask = mask.astype('float16')
        np.save(path, mask)


