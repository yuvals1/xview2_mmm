import multiprocessing
from torch.utils.data import DataLoader
import pandas as pd
import numpy as np

from ai_learner.ann import Phase, MetricsHolder

from . import ImagesDfDataset
from . import ConfusionMatrix

num_workers = multiprocessing.cpu_count() - 1


class XviewPhaseMaker:
    def __init__(self, learner, data_dir):
        self.learner = learner
        self.data_dir = data_dir

    def set_training_phase(self, df, input_img_folders, target_img_folders, target_cols=None,
                           preprocessing=None, augmentation=None, bs=8, shuffle=True):

        dataset = ImagesDfDataset(csv_file=df,
                                  data_dir=self.data_dir,
                                  input_img_folders=input_img_folders,
                                  target_img_folders=target_img_folders,
                                  target_cols=target_cols,
                                  augmentation=augmentation,
                                  preprocessing=preprocessing,
                                  )
        loader = DataLoader(dataset, batch_size=bs, shuffle=shuffle, num_workers=num_workers)
        training_phase = XviewPhase(self.learner, 'training', loader, data_dir=self.data_dir)
        return training_phase

    def set_validation_phase(self, df, input_img_folders, target_img_folders, target_cols=None,
                             preprocessing=None, augmentation=None, bs=8, shuffle=False):

        dataset = ImagesDfDataset(csv_file=df,
                                  data_dir=self.data_dir,
                                  input_img_folders=input_img_folders,
                                  target_img_folders=target_img_folders,
                                  target_cols=target_cols,
                                  augmentation=augmentation,
                                  preprocessing=preprocessing,
                                  )
        loader = DataLoader(dataset, batch_size=bs, shuffle=shuffle, num_workers=num_workers)
        validation_phase = XviewPhase(self.learner, 'validation', loader, data_dir=self.data_dir)
        return validation_phase

    def set_inference_phase(self, df, input_img_folders, preprocessing):

        dataset = ImagesDfDataset(csv_file=df,
                                  data_dir=self.data_dir,
                                  input_img_folders=input_img_folders,
                                  target_img_folders=None,
                                  target_cols=None,
                                  augmentation=None,
                                  preprocessing=preprocessing,
                                  )
        loader = DataLoader(dataset, batch_size=1, shuffle=False, num_workers=num_workers)
        inference_phase = XviewPhase(self.learner, 'inference', loader, data_dir=self.data_dir)
        return inference_phase


class XviewPhase(Phase):
    def __init__(self, learner, name, loader, data_dir):
        super().__init__(learner, name)
        self.data_dir = data_dir
        self.loader = loader
        metrics = [learner.loss] + learner.metrics
        main_metric = learner.main_metric
        self.metrics_holder = XviewMetricsHolder(metrics, main_metric)


class XviewMetricsHolder(MetricsHolder):
    cm_df = None

    def batch_ended_update(self, pr, gt):
        for metric, metric_name in zip(self.metrics, self.batches_history.keys()):
            self.batches_history[metric_name].append(metric.batch_update(pr, gt))

    def epoch_ended_update(self):
        for metric, metric_name in zip(self.metrics, self.batches_history.keys()):
            self.epochs_history[metric_name].append(metric.epoch_update(self.batches_history[metric_name]))
            self.batches_history[metric_name] = []

        if self.main_metric_name:
            self.update_main_metric_best_score()
        self.create_history_df()

    def create_batches_df(self):
        for metric, metric_name in zip(self.metrics, self.batches_history.keys()):
            if isinstance(metric, ConfusionMatrix):
                self.cm_df = pd.DataFrame({'cm': self.batches_history[metric_name]}). \
                    reset_index().rename(columns={'index': 'batch'})
                self.cm_df['num_pixels'] = self.cm_df['cm'].apply(lambda x: np.sum(x))
                self.cm_df['true_all_ratio'] = self.cm_df['cm'].apply(lambda x: np.sum(x.diagonal()) / np.sum(x))
                self.cm_df['wrong_count'] = self.cm_df['cm'].apply(lambda x: np.sum(x) - np.sum(x.diagonal()))
                for i in range(4):
                    self.cm_df['fp'+str(i+1)] = self.cm_df['cm'].apply(lambda x: np.sum(x[i, :]) - x[i, i])
                for i in range(4):
                    self.cm_df['fn' + str(i+1)] = self.cm_df['cm'].apply(lambda x: np.sum(x[:, i]) - x[i, i])

                break






